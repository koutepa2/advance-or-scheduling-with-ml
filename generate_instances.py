"""
This file serves as a random generator of synthetic instances with blocks, surgeons and
patients used for the OR scheduling. Number of instances to be generated can be selected.
"""

import json
import os
import random
import shutil
import utilities
import scipy
import numpy as np


def prepare_release_date(n_days, n_patients, prepared_patients_ratio):
    """
    Generate release date for every patient. We model the arrival of patient as a Poisson
    process (how many times an event can happen in a specified time) with lambda parameter
    (mean value) equal to (# of unscheduled patients / time horizon).

    We assume that certain ratio of patient (prepared_patients_ratio) is prepared prior
    the start of the time horizon, meaning prepared on day 0.

    Every day independently, we extract one value from Poisson distribution asking for number
    of patients that will arrive on this day. Then we randomly select this number of patients
    that are still not scheduled. We iterate over the days until all patients are not scheduled.

    :param n_days: number of days to randomly sample from
    :param n_patients: number of patients to randomly sample from
    :param prepared_patients_ratio: ratio of how many patients are prepared prior the horizon
    :return release_days: dictionary of release date assigned to every patient
    """

    # prepare the configuration
    n_prepared_patients = int(prepared_patients_ratio * n_patients)
    poisson_lambda = (n_patients - n_prepared_patients) / (n_days)

    # prepare arrays for storing prepared patients and release days
    release_days = {}
    unprepared_patients = set([i for i in range(n_patients)])

    # prepare initial set of patients prepared on day 0
    prepared_patients = random.sample(list(unprepared_patients), n_prepared_patients)
    unprepared_patients = unprepared_patients - set(prepared_patients)
    for p in prepared_patients:
        release_days[p] = 0

    # while there are unscheduled patients...
    while len(release_days) < n_patients:

        # schedule the rest of the patients
        for day in range(1, n_days):

            # extract number of patients arriving on this day from Poisson distribution
            n_patients_day = np.random.poisson(lam=poisson_lambda)

            # pick this number of random patients from the list of not scheduled patients
            if n_patients_day > len(unprepared_patients):
                assigned_patients = list(unprepared_patients)
            else:
                assigned_patients = random.sample(list(unprepared_patients), n_patients_day)

            # assign them the day and remove from unprepared patients
            unprepared_patients = unprepared_patients - set(assigned_patients)
            for p in assigned_patients:
                release_days[p] = day

    return release_days


def sample_blocks(number_of_days, number_of_rooms):
    """
    Samples OR blocks randomly.
        > fixed parameters: overtime, capacity

    :param number_of_days: number of days to randomly sample from
    :param number_of_rooms: number of rooms to randomly sample from
    :return: sampled block (with overtime, capacity, day and room)
    """

    blocks = []
    for day in range(number_of_days):
        for room in range(number_of_rooms):
            block = {
                "overtime": 60,
                "capacity": 240,
                "day": day,
                "room": room
            }

            # we have two blocks every day
            blocks.append(block)
            blocks.append(block)
    return blocks


def sample_surgeons(number_of_surgeons, number_of_days, blocks):
    """
    Samples surgeon object randomly.
        > fixed parameters: ratio of assigned blocks

    :param number_of_surgeons: number of surgeons to randomly sample from
    :param number_of_days: number of days to randomly sample from
    :param blocks: blocks to randomly sample from
    :return: sampled surgeon (with blocks)
    """
    week_length = 5
    blocks_to_sample = [b for b in blocks if b['day'] < week_length]

    surgeons = []
    for surgeon in range(number_of_surgeons):
        assignment_ratio = 1/3
        sampled_blocks = set()

        # add blocks from the first week with the given ratio
        while len(sampled_blocks) < int(len(blocks_to_sample) * assignment_ratio):
            sampled_blocks.add(random.randint(0, len(blocks_to_sample) - 1))
        sampled_blocks = list(sampled_blocks)

        # add blocks from other weeks following the pattern from the first week
        adding_blocks = []
        for i in range(week_length, number_of_days, week_length):
            for s in sampled_blocks:
                adding_blocks.append(s + i)
        sampled_blocks.extend(adding_blocks)

        surgeon = {
            "blocks": sampled_blocks
        }
        surgeons.append(surgeon)
    return surgeons


def sample_patients(number_of_days, number_of_surgeons, number_of_patients):
    """
    Samples patient object with its assignment to surgeon.
        > fixed parameters: surgery duration range, patient priority, patient group

    :param number_of_days: number of days to randomly sample from
    :param number_of_surgeons: number of surgeons to randomly sample from
    :param number_of_patients: number of patients to randomly sample from
    :return: sampled surgeon (with release day, surgery duration, patient priority, surgeon and patient's group)
    """

    # prepare release dates for all the patients
    release_dates = prepare_release_date(number_of_days, number_of_patients, prepared_patients_ratio=0.7)

    patients = []
    for patient in range(number_of_patients):

        # sample surgery duration for the patient
        sigma, mu = 0.5, 1
        surgery_duration = scipy.stats.lognorm.rvs(sigma, loc=0, scale=mu) * 60
        while surgery_duration > 240:
            surgery_duration = scipy.stats.lognorm.rvs(sigma, loc=0, scale=mu) * 60
        surgery_duration = int(round(surgery_duration / 5.0) * 5.0)

        patient = {
            "release": release_dates[patient],
            "duration": surgery_duration,
            "priority": random.choices([1, 2], weights=[0.8, 0.2])[0],
            "surgeon": random.randint(0, number_of_surgeons - 1),
            "group": random.randint(0, 5)
        }
        patients.append(patient)
    return patients


def sample_instance(number_of_days, number_of_rooms, number_of_surgeons, number_of_patients):
    """
    Samples instance object with all the sampled blocks, surgeons and patients.
        > fixed parameters: maximum blocks assigned to a surgeon per day,
        weights m1, m5, m6, m7, rooms' overtime, sequence duration

    :param number_of_days: number of days to randomly sample from
    :param number_of_rooms: number of rooms to randomly sample from
    :param number_of_surgeons: number of surgeons to randomly sample from
    :param number_of_patients: number of patients to randomly sample from
    :return: sampled instance
    """

    # sample blocks
    blocks = sample_blocks(number_of_days, number_of_rooms)

    # sample surgeons
    surgeons = sample_surgeons(number_of_surgeons, number_of_days, blocks)

    # sample patients
    patients = sample_patients(number_of_days, number_of_surgeons, number_of_patients)

    instance = {
        "number_of_days": number_of_days,
        "max_blocks_surgeon_day": 2 * number_of_rooms,
        "weights": {
            "m1": 10,
            "m5": 100,
            "m6": 10,
            "m7": 1,
            "m8": 1
        },
        "rooms": [{"overtime": 90} for _ in range(number_of_rooms)],
        "blocks": blocks,
        "surgeons": surgeons,
        "patients": patients,
        "sequence_duration": [
            [0, 20, 25, 30, 35, 40],
            [20, 0, 20, 25, 30, 35],
            [25, 20, 0, 20, 25, 30],
            [30, 25, 20, 0, 20, 25],
            [35, 30, 25, 20, 0, 20],
            [40, 35, 30, 25, 20, 0]
        ]
    }

    return instance


def main(path, number_of_instances):
    """
    Main function to be called.
        > fixed parameters: number of days, number of rooms, number of surgeons, number of patients (array)

    :param path: path to the folder where the generated instances should be stored
    :param number_of_instances: number of instances to be generated
    """
    # create folder for instance combinations
    if os.path.exists(path):  # if the folder already exists, delete it
        shutil.rmtree(path)
    os.mkdir(path)

    # prepare constants
    number_of_days_array = [5, 10]
    number_of_rooms_array = [1]
    number_of_surgeons_array = [2]
    number_of_patients_array = [30]

    for number_of_days in number_of_days_array:
        for number_of_rooms in number_of_rooms_array:
            for number_of_surgeons in number_of_surgeons_array:

                # create subfolder for instances
                subfolder = os.path.join(path, f"instances_r{number_of_rooms}_s{number_of_surgeons}_d{number_of_days}")
                os.mkdir(subfolder)

                # generate N samples for every choice of number of patients
                for number_of_patients in number_of_patients_array:
                    for i in range(number_of_instances):

                        # sample the instance
                        instance = sample_instance(number_of_days, number_of_rooms, number_of_surgeons, number_of_patients)

                        # save the instance
                        with open(os.path.join(subfolder, f'{i}_p{number_of_patients}.json'), 'w') as outfile:
                            json.dump(instance, outfile)


if __name__ == '__main__':

    # parse arguments
    arguments = utilities.parse_arguments()

    # generate instances
    main(arguments.folder, arguments.number_of_instances)
