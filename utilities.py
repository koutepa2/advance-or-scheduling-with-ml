"""
This file serves as a collector of small miscellaneous utilities used by other files.
"""

import argparse
import json
import yaml
import gurobipy as g
import graphviz
import pickle
import time
import math


class Color:
    """
    A class that defines colours for bash printing.
    """
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


class Graph:
    """
    A class that defines graph description used by Graphviz for plotting.
    """
    def __init__(self):
        self.nodes = {}
        self.edges = []
        self.current_index = 0

    def reset(self):
        self.__init__()

    def add_node(self, idx, depth):
        self.nodes[idx] = {
            'method': '',
            'name': idx,
            'obj_val': 'N',
            'integer_obj_val': 'N',
            'incumbent_solution': 'N',
            'cpu_time': 'N',
            'total_patterns': 'N',
            'added_patterns': 'N',
            'subproblems_avg': 'N',
            'depth': depth,
            'RC_fixing': 'N',
            'color': 'black'
        }
        self.current_index += 1

    def add_edge(self, start, end, decision):
        self.edges.append(
            {
                'start': start,
                'end': end,
                'decision': decision
            }
        )

    def update_node(self, node, updates):
        for update_object, update_value in updates.items():
            self.nodes[node][update_object] = update_value

    def plot_tree(self, tree_name):

        # create Graphviz object for defining the graph
        g = graphviz.Digraph(filename=tree_name, format="pdf")

        # add all the nodes
        for key, node in self.nodes.items():
            g.node(node['name'],
                   label=f"{node['method']} {node['name']} (depth {node['depth']})\nobjective value: {node['obj_val']}\ninteger objective value: {node['integer_obj_val']}"
                         f"\nincumbent solution: {node['incumbent_solution']}\nCPU time: {node['cpu_time']}\ntotal patterns: {node['total_patterns']}\nadded patterns: {node['added_patterns']}\nsubproblems avg: {node['subproblems_avg']}"
                         f"\nRC fixing: {node['RC_fixing']}",
                   fontname='CMU serif', color=node['color'])

        # add all the edges
        for edge in self.edges:
            g.edge(edge['start'], edge['end'], xlabel=edge['decision'], fontsize='8', fontname='CMU serif')

        # sav the graph
        g.save()
        g.render(filename=tree_name, view=0, cleanup=1)


class Analysis:
    """
    A class that defines various parameters stored during the analysis and subproblem feature recording.
    """
    def __init__(self):
        self.time_start = 0
        self.total_cpu_time = 0
        self.mp_cpu_time = 0
        self.mh_cpu_time = 0
        self.sp_cpu_time = 0
        self.ml_record_time = 0
        self.ml_predict_time = 0
        self.ml_overall_time = 0
        self.number_of_nodes = 0
        self.number_of_cg_iterations = 0
        self.number_of_subproblems = 0
        self.subproblem_features = []
        self.maximum_depth = -1

    def reset(self):
        self.__init__()

    def find_maximum_depth(self, nodes):
        maximum_depth = max([node['depth'] for _, node in nodes.items()])
        self.maximum_depth = maximum_depth

    def print(self):
        print('\n' + Color.PURPLE + f"{'#' * 50}")
        print('# ANALYSIS')
        print(f"{'#' * 50}" + Color.END + '\n')
        print(f"Total CPU time: {self.total_cpu_time} seconds")
        print(f"Master problem CPU time: {self.mp_cpu_time} seconds")
        print(f"Master heuristic CPU time: {self.mh_cpu_time} seconds")
        print(f"Subproblem CPU time: {self.sp_cpu_time} seconds")
        print(f"ML time: {self.ml_overall_time} (record: {self.ml_record_time}\tpredict: {self.ml_predict_time})")
        print()
        print(f"Number of nodes: {self.number_of_nodes}")
        print(f"Number of CG iterations: {self.number_of_cg_iterations}")
        print(f"Number of subproblems solved: {self.number_of_subproblems}")
        print(f"Maximum depth: {self.maximum_depth}")


class Solution:
    def __init__(self):
        self.type = None
        self.mp_index = 0
        self.objective_value = math.inf
        self.optimality_gap = 0
        self.variables = None
        self.patterns = []
        self.solution_found = time.time()

    def reset(self):
        self.__init__()

    def update(self, type, mp_index, objective_value, optimality_gap, variables, patterns, solution_found):
        self.type = type
        self.mp_index = mp_index
        self.objective_value = objective_value
        self.optimality_gap = optimality_gap
        self.variables = variables
        self.patterns = patterns
        self.solution_found = solution_found

    def print(self, instance):
        print('\n' + Color.PURPLE + f"{'#' * 50}", flush=True)
        print(f'# SOLUTION ({self.type})', flush=True)
        print(f"{'#' * 50}" + Color.END + '\n', flush=True)

        if len(self.patterns) == 0:
            print("No integral solution found before timeout!")
            return

        if self.type == 'original':
            for b in instance.blocks:
                patients = [instance.get_patient(idx) for idx, item in enumerate(self.variables['x'][:, b.id]) if item == 1]
                print(
                    f"| BLOCK: {b.id} | PATIENTS: {[patient.id for patient in patients]} | SURGEONS: {list(set([patient.surgeon for patient in patients]))} | OVERTIME: {self.variables['o'][b.id]} | UNDERTIME: {self.variables['z'][b.id]}", flush=True)
        else:
            for block in instance.blocks:
                pattern = [p for p in self.patterns if p.block.id == block.id and round(self.variables['theta'][p.id], 2) == 1][0]
                pattern.print_pattern()

        print(f"OBJECTIVE VALUE: {self.objective_value}", flush=True)
        print(f"OPTIMALITY GAP: {self.optimality_gap}", flush=True)
        print(f"Incumbent solution found after {self.solution_found} seconds")
        print(flush=True)


class Parameters:
    """
    A class that serves as a collector for parameters and structures used in the BAP algorithm.
    """

    def __init__(self):
        self.analysis = Analysis()
        self.graph = Graph()
        self.solution = Solution()
        self.batch_index = 0
        self.type_of_validation, self.type_of_model = None, None
        self.threshold_depth, self.model = None, None
        self.k = None
        self.subproblem_strategy, self.sorting_strategy = None, None

    def load_arguments(self, arguments):
        self.type_of_validation = arguments['type_of_validation']
        self.type_of_model = arguments['type_of_model']
        self.threshold_depth = arguments['threshold_depth']
        self.k = arguments['k']
        self.subproblem_strategy = arguments['subproblem_strategy']
        self.sorting_strategy = arguments['sorting_strategy']

        if arguments['model_path']:
            self.model = pickle.load(open(arguments['model_path'], 'rb'))


def parse_arguments():
    """
    This function parses the input arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--instance', '-i', help='path to the instance')
    parser.add_argument('--folder', '-f', help='path to the folder where the generated instances should be stored')
    parser.add_argument('--number_of_instances', '-n', type=int, help='number of instances to be generated')
    args = parser.parse_args()
    return args


def parse_arguments_from_file():
    """
    This function parses the input arguments from specified text file.
    """
    f = open('parameters.yaml', 'rb')
    args = yaml.load(f, Loader=yaml.FullLoader)
    return args


def parse_json(path):
    """
    This function parses the input JSON file.

    :param path: path to the JSON file
    """
    f = open(path)
    data = json.load(f)
    return data


def store_model_representation(model, model_name):
    """
    This function stores representation and solution of a given Gurobi model to a file.  If the model is infeasible, it stores
    the smallest possible infeasible subset of the model. If the model is neither optimal nor infeasible, warning with model
    status is printed. All representations are stored in the same folder as source code.

    :param model: optimized Gurobi model
    :param model_name: name of the file to which should be the ouput written
    """

    # store model representation
    model.write(f'model_{model_name}_representation.lp')

    if model.Status == g.GRB.OPTIMAL:

        # store model solution if the model was solved to optimality
        model.write(f'model_{model_name}_solution.sol')

    elif model.Status == g.GRB.INFEASIBLE or model.Status == g.GRB.INF_OR_UNBD:

        # store smallest possible infeasible subset of infeasible model
        model.computeIIS()
        model.write(f'model_{model_name}_infeasibility.ilp')

    else:
        print(f"WARNING: Solution of model {model_name} is neither optimal nor infeasible, model status is {str(model.Status)}")


def convert_dictionary_to_array(dictionary, array):
    """
    This function converts dictionary of multidimensional variable and its values in the format of Gurobi model
    getAttr('X', variable) method to corresponding variable array of values.

    :param dictionary: dictionary of multidimensional variable and its values in the format Gurobi model getAttr('X', variable) method
    :param array: destination array with the size of variable's dimensions
    """
    for ((i, j), v) in dictionary.items():
        array[i, j] = v
    return array
