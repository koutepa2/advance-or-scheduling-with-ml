import os
import json
import pandas as pd
import argparse
import random
import scipy
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import time
import utilities, solve_ilp, solve_bap, prepare_instance

matplotlib.pyplot.rcParams.update({
    "text.usetex": True,
    'font.size': 15,
    "font.family": "serif",
    "axes.grid" : True,
    "grid.color": "#e6e6e6",
    "axes.axisbelow": True
})
custom_blue, custom_red = "#92C5DE", "#B2182B"


"""
CHECK INSTANCE
"""
# path = 'instances/evaluating/instances_r1_s4_d20/1_p100.json'
# with open(path, 'r') as f:
#   data = json.load(f)
#   release = []
#   duration = []
#   for p in data['patients']:
#       release.append(p['release'])
#       duration.append(p['duration'])
#
# plt.figure(1)
# plt.hist(x=duration, bins=240, color='red', alpha=0.7, rwidth=2)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Surgery duration (minutes)')
# plt.ylabel('Frequency')
# plt.title('Histogram of surgery durations')
# plt.xlim(0, 240)
# plt.show()
#
# plt.figure(2)
# plt.hist(x=release, bins=20, color='blue', alpha=0.7, rwidth=0.85)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Release date (days)')
# plt.ylabel('Frequency')
# plt.title('Histogram of release date')
# plt.xlim(-1, 20)
# plt.show()


"""
SIMULATION OF SURGERY DURATION
"""
# # loc = gets subtracted from  data so that 0 becomes the infimum of the range of the data
# # scale = exp(mu); mu = sample mean of the log of the data
# # shape =  standard deviation of the log of the data.

# sigma, mu = 0.5, 1
# d1 = scipy.stats.lognorm.rvs(sigma, loc=0, scale=mu, size=10000) * 60
# d2 = [int(round(x/5.0)*5.0) for x in d1 if x <= 240]
#
# plt.figure(1)
# plt.hist(x=d2, bins=240, color=custom_red, alpha=1, rwidth=2)
# plt.hist(x=d1, bins=240, color=custom_blue, alpha=1, rwidth=1)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Duration [minute]')
# plt.ylabel('Frequency [-]')
# # plt.title('Histogram of surgery durations')
# plt.xlim(0, 250)
# plt.savefig("param_surgery_duration.pdf", format="pdf", bbox_inches="tight")
# plt.show()


"""
SIMULATION OF WAITING TIME
"""
# Poisson distribution = estimates how many times an event can happen in a specified time
#   - we model the arrival of patients as a Poisson process with lambda
#   - every day independently, we extract one value from Poisson distribution asking for number
#     of patients that will arrive on this day
#   - we then randomly select those patients, that are still not scheduled
#   - we iterate over the days until all patients are not scheduled
#
# # prepare the configuration
# n_days, n_patients, prepared_patients_ratio = 20, 10000, 0.7
# n_prepared_patients = int(prepared_patients_ratio * n_patients)
# poisson_lambda = (n_patients - n_prepared_patients) / (n_days)
# print('Poisson lambda: ', poisson_lambda)
#
# # prepare arrays for storing prepared patients and release days
# release_days = {}
# unprepared_patients = set([i for i in range(n_patients)])
#
# # prepare initial set of patients prepared on day 0
# prepared_patients = random.sample(list(unprepared_patients), n_prepared_patients)
# unprepared_patients = unprepared_patients - set(prepared_patients)
# for p in prepared_patients:
#     release_days[p] = 0
#
# # while there are unscheduled patients...
# while len(release_days) < n_patients:
#     # schedule the rest of the patients
#     for day in range(1, n_days):
#
#         # extract number of patients arriving on this day from Poisson distribution
#         n_patients_day = np.random.poisson(lam=poisson_lambda)
#         print(day, n_patients_day)
#
#         # pick this number of random patients from the list of not scheduled patients
#
#         if n_patients_day > len(unprepared_patients):
#             assigned_patients = list(unprepared_patients)
#         else:
#             assigned_patients = random.sample(list(unprepared_patients), n_patients_day)
#
#         # assign them the day and remove from unprepared patients
#         unprepared_patients = unprepared_patients - set(assigned_patients)
#         for p in assigned_patients:
#             release_days[p] = day
#
# print(len(release_days))
# release_array = [value for _, value in release_days.items()]
#
# plt.figure(2)
# plt.hist(x=release_array, bins=20, color=custom_blue, alpha=1, rwidth=0.85)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Release date [day]')
# plt.ylabel('Frequency [-]')
# # plt.title('Histogram of release date')
# plt.xlim(-1, 20)
# plt.savefig("param_release_date.pdf", format="pdf", bbox_inches="tight")
# plt.show()


"""
COMPARE INSTANCES
"""
# folder = 'instances/record_small/instances_r1_s2_d5'
# instances_path = [os.path.join(folder, f) for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and f.endswith('.json')]
#
# start_time = time.time()
# names, ilp_obj, bap_obj = [], [], []
# for instance in instances_path:
#     print(instance)
#     # _, solution_ilp = solve_ilp.main(instance)
#     _, solution_bap, _ = solve_bap.main(instance, utilities.parse_arguments_from_file())
#     names.append(instance)
#     # ilp_obj.append(round(solution_ilp.objective_value,2))
#     bap_obj.append(round(solution_bap.objective_value,2))
#
# # for i in range(len(names)):
# #     print(f"{names[i]}\t\t{ilp_obj[i]}\t\t{bap_obj[i]}\t\t{'NO' if ilp_obj[i] != bap_obj[i] else ''}")


"""
COMPUTE LOAD FACTOR
"""
# folder = 'instances/experiments_quadratic_2/bap_before'
# subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
#
# for subfolder_path in subfolders:
#     instances = ['2_p60.json', '2_p100.json', '1_p100.json', '2_p80.json', '0_p100.json', '0_p80.json', '0_p60.json',
#                  '1_p80.json', '1_p60.json']
#     instances_path = [os.path.join(subfolder_path, i) for i in instances]
#     print(subfolder_path)
#     for instance_path in instances_path:
#         instance = prepare_instance.main(instance_path)
#
#         # load factor = total surgery time / available time
#         load_factor = sum([patient.duration for patient in instance.patients]) / (instance.n_blocks * instance.blocks[0].capacity)
#         print(round(load_factor, 2))


"""
EDIT JSON (add feature)
"""
# folder = 'instances/ilp_after_overtime_undertime_sequence'
# subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
# json_files = []
# for subfolder in subfolders:
#     json_files.extend([f.path for f in os.scandir(subfolder) if f.path.endswith('.json')])
#
# for path in json_files:
#     with open(path, 'r') as f:
#         data = json.load(f)
#         data["sequence_duration"] = [
#             [0, 20, 25, 30, 35, 40],
#             [20, 0, 20, 25, 30, 35],
#             [25, 20, 0, 20, 25, 30],
#             [30, 25, 20, 0, 20, 25],
#             [35, 30, 25, 20, 0, 20],
#             [40, 35, 30, 25, 20, 0]
#         ]
#         for i in data['patients']:
#             i['group'] = random.randint(0, 5)
#     with open(path, 'w') as f:
#         json.dump(data, f)


"""
EDIT CSV
"""
# path = 'instances/record_instances_2_ranking/instances_r1_s8_d8/subproblem_features_recording.csv'
# days = 8
# batch_size = 2 * days
#
# # open CSV
# df = pd.read_csv(path)
#
# # prepare column vector with batches
# batches = []
# print(len(df)/batch_size)
# for b in range(int(len(df)/batch_size)):
#     for i in range(batch_size):
#         batches.append(b)
#
# # add batches as new column
# df['batch index'] = batches
#
# # save CSV
# df.to_csv(path, index=False)


"""
MERGE CSV
"""
# # define folder to search in
# folder = "instances/record_small_mp/"
#
# # 1. define final CSV file
# csv_name = 'subproblem_features_recording.csv'
# final_csv_path = os.path.join(folder, csv_name)
#
# # 2. list all subfolder's CSV files
# subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
# subfolder_csv_files = [os.path.join(subfolder_path, csv_name) for subfolder_path in subfolders if os.path.exists(os.path.join(subfolder_path, csv_name))]
#
# # 3. merge them together and save final CSV
# subfolder_dfs = []
# batch_increase = 0
# for f in subfolder_csv_files:
#     print(f'Adding file {f}...')
#     df = pd.read_csv(f,low_memory=False)
#
#     # in merging datasets, get unique value of batch index, if the column is present
#     if 'batch index' in df.columns:
#         batch_increase_new = max(df['batch index']) + 1
#         df['batch index'] = df['batch index'] + batch_increase
#         batch_increase += batch_increase_new
#
#     subfolder_dfs.append(df)
#
# df_concat = pd.concat(subfolder_dfs, ignore_index=True)
# df_concat.to_csv(final_csv_path, index=False)
