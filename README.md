# Advance scheduling problem in operating room planning with the guidance of machine learning

The goal of this project is to analyse the surgical schedule provided by University Hospital Hradec Králové and propose a solution for automatic and more efficient scheduling of operating rooms.

We propose to solve this task by Branch And Price (BAP) algorithm. However, a lot of surgery-room assignments might occur in longer time horizon, therefore we propose Machine Learning (ML) method, namely Graph Neural Network (GNN), to help us with finding the most promising surgery-room assignments and speeding up the BAP algorithm.


## Instance generation
This part is located in the `generate_instances.py` file. It serves as a random generator of synthetic instances with blocks, surgeons and
patients used for the OR scheduling. Number of instances to be generated can be selected.

Run it as follows:
```commandline
python generate_instances.py --folder PATH_TO_FOLDER --number_of_instances N
```
or
```commandline
python generate_instances.py -f PATH_TO_FOLDER -n N
```

## Instance preparation
This part is located in the `prepare_instance.py` file. It serves as a preparation script for an input JSON file. It parses all information contained in the input 
JSON file and converts it to the format of Instance class object which is then used for the optimization. 

Apart from the parsing function, this file also declares all the class definitions for used entities - Instance, 
Room, Block, Surgeon and Patient.

Run it as follows:
```commandline
python prepare_instance.py --instance PATH_TO_INSTANCE
```
or
```commandline
python prepare_instance.py -i PATH_TO_INSTANCE
```

To use this script correctly, **input JSON file should have following structure**:
```json
{
  "number_of_days": 1,
  "max_blocks_surgeon_day": 1,
  "weights": {
    "m1": 1,
    "m5": 1,
    "m6": 1,
    "m7": 1
  },
  "rooms": [
    {
      "overtime": 30
    }
  ],
  "blocks": [
    {
      "overtime": 10,
      "capacity": 100,
      "day": 0,
      "room": 0
    }
  ],
  "surgeons": [
    {
      "blocks": [0]
    }
  ],
  "patients": [
    {
      "release": 0,
      "duration": 40,
      "priority": 1.0,
      "surgeon": 0
    }
  ]
}
```
where all parameters can be changed according to the needs of the task.


## Solving the scheduling problem

### ILP
This part is located in the `solve_ilp.py` file. It serves as a main script for performing the ILP optimization of
declared model using Gurobi solver.

Run it as follows:
```commandline
python solve_ilp.py --instance PATH_TO_INSTANCE
```
or
```commandline
python solve_ilp.py -i PATH_TO_INSTANCE
```

### Branch And Price

This part is located in multiple files:

- In `prepare_bap.py`, all the classes needed for the column generation algorithm to run properly are declared here - patterns
to be operated with, master problem and subproblem.
- In `solve_bap.py`, branch and price algorithm is solved using only primal formulation. It consist of preparing the instance using
`prepare_instance.py` script, constructing initial set of patterns and running column generation and branching in a loop till the incumbent solution is found.

Run it as follows:
```commandline
python solve_bap.py
```

<span style="color:red">**There are preference attributes that need to be configured in a file `parameters.yaml` every time before running the script
(described in detail in the section 'Analysing and recording the instances').**</span>

Pseudocode for finding the initial set of patterns:
```{python, tidy=FALSE, eval=FALSE, highlight=FALSE }
    
    patients = initialize patients
    blocks = initilaize blocks 
    patterns = []
    
    patients = sort patients by their release date
   
    for patient in patients:
         
        patient_blocks = retrieve patient's blocks
        patient_blocks = sort patient_blocks by their day
        
        for block in patient_blocks:
            if block.time >= patient.duration:
                add (block,patient) to patterns
                block.time = block.time - patient.duration
                break
        
```

## Training the model

This part is located in multiple files:

- `train_clf.py` serves as a script for training and validation of specified random forest or XGBoost model using Scikit Learn

Run it as follows:
```commandline
python train_clf.py --instance PATH_TO_DATA_FILE
```
or
```commandline
python train_clf.py -i PATH_TO_DATA_FILE
```

- `train_rank.py` serves as a script for training and validation of specified ranking model using LigthGBM

Run it as follows:
```commandline
python train_rank.py --instance PATH_TO_DATA_FILE
```
or
```commandline
python train_rank.py -i PATH_TO_DATA_FILE
```

Both those files can be used for training a desired model which is then loaded and used in the ranking task of subproblems during the branch and price algorithm.

## Analysing and recording the instances

`validate_instances.py` serves as the main script for the validation of the instances stored in some folder. It is the wrapper around all the parts described above that
runs for desired number of instances located in some folder and either record features of the subproblems or stores the results and measurements of the algorithm.

Run it as follows:
```commandline
python validate_instances.py
```

<span style="color:red">**There are preference attributes that need to be configured in a file `parameters.yaml` every time before running the script:**</span>
- **type_of_validation:**
  - `record`: record features of all the subproblems computed in every instance of BAP algorithm; one CSV
    file with recorded values is created in the provided folder (called `subproblem_features_recording.csv`)
  - `analyse ilp`: iterate over every instance and run ILP algorithm, store all the ILP algorithm features (total
    CPU time, number of subproblems solved, ...) and in the end compute stats on this data (means); CSV file called 
    `ilp_analysis.csv`is created in the provided folder
  - `analyse bap`: iterate over every instance and run BAP algorithm, store all the BAP algorithm features (total
    CPU time, number of subproblems solved, ...) and in the end compute stats on this data (means); CSV file called 
    `bap_analysis.csv` is created in the provided folder 
- **type_of_model:** type of a model that we consider - in analysis, we load model of this type when ML is in use, in record, we store features for the model of this type
  - `clf`: model defined for classification task
  - `rank sp`: model defined for ranking task where SP reduced cost is ranked
  - `rank mp`: model defined for ranking task where MP objective impact is ranked
- **instance:** path to the instance to be solved
- **folder:** path to the folder of instances to be solved
- **k:** number of subproblems to return in subproblem strategy
- **threshold_depth:** maximal branching depth of the tree (nodes deeper than this value are substituted with ILP)
- **subproblem_strategy:** strategy for selecting returned improving patterns
  - `return first improving`: return K first found patterns with positive reduced cost
  - `return first overall`: from K first solved patterns, return those with positive reduced cost
  - `return best`: return K best patterns with the highest positive reduced cost
- **sorting_strategy:** strategy for sorting blocks before searching for one with positive reduced cost
  - `random`: randomly order the blocks
  - `ranked`: send features of the patterns to ML model which ranks them
- **model_path:** path to the trained model
