"""
In this file, branch and price algorithm is solved using only primal formulation. It consists of preparing the instance using
prepare_instance.py script, constructing initial set of patterns and running column generation and branching in a loop till
the incumbent solution is found.
"""

import numpy as np
import copy
import time
import random
import gurobipy as g
import torch
from sklearn import metrics
import prepare_instance, prepare_bap, utilities


# define parameters
params = utilities.Parameters()
TIMELIMIT = 2 * 60 * 60  # TODO: set back to 4 hours


def generate_initial_patterns(instance):
    """
    This function generates initial set of feasible patterns for the column generation algorithm.
    Currently, greedy strategy is used: first, we sort patients by release date and then assign to each
    block one patient with the smallest release date until all blocks are covered. This generates an initial
    solution where one patient is assigned to every block.

    :param instance: instance on which the OR scheduling task is performed
    :return: set of created initial pattern objects
    """

    # initialize patients and patterns
    patients = instance.patients
    block_vocab = {}
    for block in instance.blocks:
        block_vocab[block.id] = {
            'patients': [],
            'surgeons': set(),
            'leftover_capacity': block.capacity
        }

    # sort patients by their release date
    patients.sort(key=lambda x: x.release)

    # iterate over all the patients
    for idx, patient in enumerate(patients):

        # retrieve patient's blocks
        patient_surgeon = instance.get_surgeon(patient.surgeon)
        patient_blocks = [instance.get_block(block) for block in patient_surgeon.blocks if instance.get_block(block).day >= patient.release]

        # sort patient's blocks by their day
        patient_blocks.sort(key=lambda x: x.day)

        # if we can still fit the patient into the block, do it
        for block in patient_blocks:
            sequence_duration = 0 if block_vocab[block.id]['patients'] == [] else instance.sequence_duration[block_vocab[block.id]['patients'][-1].group][patient.group]
            operation_duration = patient.duration + sequence_duration
            if block_vocab[block.id]['leftover_capacity'] >= operation_duration:
                block_vocab[block.id]['leftover_capacity'] -= operation_duration
                block_vocab[block.id]['patients'].append(patient)
                block_vocab[block.id]['surgeons'].add(patient_surgeon)
                break

    # create dummy pattern objects
    patterns, idx = [], 0
    for block in instance.blocks:
        pattern = prepare_bap.Pattern(idx, block, [], [], 0, block.capacity, instance.n_blocks, instance.n_patients, instance.n_surgeons)
        patterns.append(pattern)
        idx += 1

    # add generated pattern objects
    for block in instance.blocks:
        if block_vocab[block.id]['patients']:
            pattern = prepare_bap.Pattern(idx, block, block_vocab[block.id]['patients'], list(block_vocab[block.id]['surgeons']), 0, block_vocab[block.id]['leftover_capacity'], instance.n_blocks, instance.n_patients, instance.n_surgeons)
            patterns.append(pattern)
            idx += 1

    return patterns


def generate_improving_pattern(master_problem):
    """
    This function generates improving pattern/s (= pattern with positive reduced cost) for the column
    generation algorithm. If the objective value of the solved subproblem is positive, we have found
    an imporving pattern and we can return it. Currently, we can choose from multiple strategies of selecting
    a pattern that is returned. We also have multiple strategies for sorting the blocks (and therefore
    subproblems) at the beginning as well. Subproblems are then searched in the order predicted by the
    selected strategy.

    :param master_problem: current state of master problem
    :return: improving pattern/s found by the selected strategy
    """

    improving_patterns, improving_features, improving_values = [], [], []
    evaluated_blocks = 0

    # sort the block in order in which they should be searched
    if params.sorting_strategy == 'random':
        sorted_blocks, sorted_predictions = random.sample(master_problem.instance.blocks, len(master_problem.instance.blocks)), np.zeros(len(master_problem.instance.blocks))
    if params.sorting_strategy == 'ranked':
        start_time = time.time()
        sorted_blocks, sorted_predictions = rank_subproblems(master_problem)
        params.analysis.ml_overall_time += time.time() - start_time

    # if recording is performed, write down subproblem features
    if params.type_of_validation == 'record':
        record_subproblems(master_problem)

    # solve subproblem for (block, room, day)...
    for block, prediction in zip(sorted_blocks, sorted_predictions):
        room, day = master_problem.instance.get_room(block.room), block.day

        # update number of subproblems value for analysis
        params.analysis.number_of_subproblems += 1
        evaluated_blocks += 1

        # solve the subproblem for the pattern
        subproblem = prepare_bap.Subproblem(master_problem.instance)
        subproblem.optimize_model(max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), master_problem.dual_variables, master_problem.branching_constraints, block, day, room)

        # if the SP is not solved to optimality, continue with different subproblem
        if subproblem.model.status != g.GRB.OPTIMAL:
            continue

        # if the objective value is positive, create Pattern object and return it
        if subproblem.objective_value > 0:

            # crete Pattern corresponding to solved subproblem
            improving_item_patients = [master_problem.instance.get_patient(idx) for idx, item in enumerate(subproblem.variables['x']) if round(item, 2) == 1]
            improving_item_surgeons = [master_problem.instance.get_surgeon(idx) for idx, item in enumerate(subproblem.variables['n']) if round(item, 2) == 1]
            improving_pattern = prepare_bap.Pattern(max([pattern.id for pattern in master_problem.instance.patterns + improving_patterns]) + 1 if master_problem.instance.patterns + improving_patterns else 0,
                                                    block, improving_item_patients, improving_item_surgeons, subproblem.variables['o'], subproblem.variables['z'],
                                                    master_problem.instance.n_blocks, master_problem.instance.n_patients, master_problem.instance.n_surgeons)
            improving_patterns.append(improving_pattern)
            improving_values.append(subproblem.objective_value)

        # return first improving pattern
        if params.subproblem_strategy == 'return first improving' and len(improving_patterns) == params.k:
            return improving_patterns

        if params.subproblem_strategy == 'return first overall' and evaluated_blocks >= params.k:
            if len(improving_patterns) > 0:
                return improving_patterns

    # return K best improving patterns
    if params.subproblem_strategy == 'return best':
        k = params.k
        sorted_indices = np.argsort(np.asarray(improving_values))[::-1]
        sorted_indices = sorted_indices if len(sorted_indices) < k else sorted_indices[:k]
        sorted_improving_patterns = [improving_patterns[i] for i in sorted_indices]
        return sorted_improving_patterns

    return improving_patterns


def record_subproblem(master_problem, block, day, sp_objective_value, mp_impact):
    """
    This function records all features of a described subproblem and return them in compact format.

    :param master_problem: master problem instance
    :param block: block that is considered in the subproblem
    :param day: day that is considered in the subproblem
    :param sp_objective_value: objective value of the subproblem
    :param mp_impact: value of the impact the subproblem has on the master problem
    :return: features of a subproblem serving either for storing it to CSV or using it as an input to the model
    """

    instance, branching_constraints, primal_variables, dual_variables = master_problem.instance, master_problem.branching_constraints, master_problem.primal_variables, master_problem.dual_variables

    # prepare values used for the bins in histogram recording
    clinical_priority_bins = 2
    # waiting_days_bins = [-20, 0, 5, 10, 15, 20]  # TODO: change bins according to size of trained samples - bins for small instances
    waiting_days_bins = [-20, 0, 2, 4, 6, 10]  # bins for small instances
    surgery_time_bins = [0, 30, 60, 90, 120, 150, 180, 600]
    setup_time_bins = [0, 1, 2, 3, 4, 5, 6]

    # prepare patients and surgeons that cannot be assigned to the block due to the applied branching rules
    branch_deleted_patients = [patient for (patient, p_block, indication) in branching_constraints['patient'] if p_block == block.id and indication == 0]
    branch_deleted_surgeons = [surgeon for (surgeon, s_day, indication) in branching_constraints['surgeon'] if s_day == day and indication == 0]

    # prepare patterns and surgeons that are assigned to the block
    block_patterns = [pattern for pattern in instance.patterns if pattern.block.id == block.id]
    block_surgeons = [surgeon.id for surgeon in instance.surgeons if block.id in surgeon.blocks and surgeon.id not in branch_deleted_surgeons]

    # -- patients and patterns that are fully involved in the block and surgeons that are fully involved in the day (integral)
    sumproducts = {patient.id: sum([(1 if patient.id in pattern.patient_id else 0) * primal_variables['theta'][pattern.id] for pattern in block_patterns]) for patient in instance.patients}
    fully_involved_patients = [patient for patient in instance.patients if round(sumproducts[patient.id], 2) == 1]
    fully_involved_patterns = [pattern for pattern in block_patterns if round(primal_variables['theta'][pattern.id], 2) == 1]
    fully_involved_surgeons = [surgeon for surgeon in instance.surgeons if round(primal_variables['n'][surgeon.id, day], 2) == 1]

    # -- patients and patterns that are partially involved in the block and surgeons that are partially involved in the day (fractional)
    partially_involved_patients = [patient for patient in instance.patients if round(sumproducts[patient.id], 2) > 0 and round(sumproducts[patient.id], 2) < 1]
    partially_involved_patterns = [pattern for pattern in block_patterns if round(primal_variables['theta'][pattern.id], 2) > 0 and round(primal_variables['theta'][pattern.id], 2) < 1]
    partially_involved_surgeons = [surgeon for surgeon in instance.surgeons if round(primal_variables['n'][surgeon.id, day], 2) > 0 and round(primal_variables['n'][surgeon.id, day], 2) < 1]

    # -- patterns, surgeons and patients that can be involved in the block
    possible_patterns = [pattern for pattern in instance.patterns if pattern.block.id == block.id and pattern not in fully_involved_patterns and pattern not in partially_involved_patterns]
    possible_surgeons = [surgeon for surgeon in instance.surgeons if block.id in surgeon.blocks and surgeon.id not in branch_deleted_surgeons and surgeon not in fully_involved_surgeons and surgeon not in partially_involved_surgeons]
    possible_patients = [patient for patient in instance.patients if patient.release <= block.day and patient.surgeon in block_surgeons and patient.id not in branch_deleted_patients and patient not in fully_involved_patients and patient not in partially_involved_patients]

    # -- is the schedule w.r.t. the block integral or not?
    possible_patterns_theta = [primal_variables['theta'][pattern.id] for pattern in block_patterns]
    is_block_integral = 1
    for p_value in possible_patterns_theta:
        if not round(p_value, 2).is_integer():
            is_block_integral = 0
            break

    # -- total load of the block (integral and fractional)
    block_load = sum([patient.duration for patient in fully_involved_patients])
    block_load_fractional = sum([patient.duration * sumproducts[patient.id] for patient in partially_involved_patients])

    # -- clinical priority of the block (integral and fractional and possible)
    clinical_priority = 0 if not fully_involved_patients else sum([patient.priority for patient in fully_involved_patients]) / len(fully_involved_patients)
    clinical_priority_hist, _ = np.histogram([patient.priority for patient in fully_involved_patients], bins=clinical_priority_bins)
    clinical_priority_fractional = 0 if not partially_involved_patients else sum([patient.priority for patient in partially_involved_patients]) / len(partially_involved_patients)
    clinical_priority_fractional_hist, _ = np.histogram([patient.priority for patient in partially_involved_patients], bins=clinical_priority_bins)
    clinical_priority_possible = 0 if not possible_patients else sum([patient.priority for patient in possible_patients]) / len(possible_patients)
    clinical_priority_possible_hist, _ = np.histogram([patient.priority for patient in possible_patients], bins=clinical_priority_bins)

    # -- number of waiting days of the block (integral and fractional and possible)
    waiting_days = 0 if not fully_involved_patients else sum([day - patient.release for patient in fully_involved_patients]) / len(fully_involved_patients)
    waiting_days_hist, _ = np.histogram([day - patient.release for patient in fully_involved_patients], bins=waiting_days_bins)
    waiting_days_fractional = 0 if not partially_involved_patients else sum([day - patient.release for patient in partially_involved_patients]) / len(partially_involved_patients)
    waiting_days_fractional_hist, _ = np.histogram([day - patient.release for patient in partially_involved_patients], bins=waiting_days_bins)
    waiting_days_possible = 0 if not possible_patients else sum([day - patient.release for patient in possible_patients]) / len(possible_patients)
    waiting_days_possible_hist, _ = np.histogram([day - patient.release for patient in possible_patients], bins=waiting_days_bins)

    # -- surgery time of the block (integral and fractional and possible)
    surgery_time = 0 if not fully_involved_patients else sum([patient.duration for patient in fully_involved_patients]) / len(fully_involved_patients)
    surgery_time_hist, _ = np.histogram([patient.duration for patient in fully_involved_patients], bins=surgery_time_bins)
    surgery_time_fractional = 0 if not partially_involved_patients else sum([patient.duration for patient in partially_involved_patients]) / len(partially_involved_patients)
    surgery_time_fractional_hist, _ = np.histogram([patient.duration for patient in partially_involved_patients], bins=surgery_time_bins)
    surgery_time_possible = 0 if not possible_patients else sum([patient.duration for patient in possible_patients]) / len(possible_patients)
    surgery_time_possible_hist, _ = np.histogram([patient.duration for patient in possible_patients], bins=surgery_time_bins)

    # -- patient set-up time of the block (integral and fractional and possible)
    setup_time = 0 if not fully_involved_patients else sum([patient.group for patient in fully_involved_patients]) / len(fully_involved_patients)
    setup_time_hist, _ = np.histogram([patient.group for patient in fully_involved_patients], bins=setup_time_bins)
    setup_time_fractional = 0 if not partially_involved_patients else sum([patient.group for patient in partially_involved_patients]) / len(partially_involved_patients)
    setup_time_fractional_hist, _ = np.histogram([patient.group for patient in partially_involved_patients], bins=setup_time_bins)
    setup_time_possible = 0 if not possible_patients else sum([patient.group for patient in possible_patients]) / len(possible_patients)
    setup_time_possible_hist, _ = np.histogram([patient.group for patient in possible_patients], bins=setup_time_bins)

    # -- number of patient branching constraints that involve this block
    patient_branching_constraints_0 = [patient for (patient, p_block, indication) in branching_constraints['patient'] if p_block == block.id and indication == 0]
    patient_branching_constraints_1 = [patient for (patient, p_block, indication) in branching_constraints['patient'] if p_block == block.id and indication == 1]

    # -- number of surgeon branching constraints that involve this block
    surgeon_branching_constraints_0 = [surgeon for (surgeon, s_day, indication) in branching_constraints['surgeon'] + branching_constraints['aggregated_surgeon'] + branching_constraints['extended_aggregated_surgeon'] if s_day == day and indication == 0]
    surgeon_branching_constraints_1 = [surgeon for (surgeon, s_day, indication) in branching_constraints['surgeon'] + branching_constraints['aggregated_surgeon'] + branching_constraints['extended_aggregated_surgeon'] if s_day == day and indication == 1]

    subproblem_features = {
        '# instance days': instance.n_days,
        '# instance surgeons': instance.n_surgeons,
        '# instance patients': instance.n_patients,

        '# fully involved patients': len(fully_involved_patients),
        '# fully involved patterns': len(fully_involved_patterns),
        '# fully involved surgeons': len(fully_involved_surgeons),
        '# partially involved patients': len(partially_involved_patients),
        '# partially involved patterns': len(partially_involved_patterns),
        '# partially involved surgeons': len(partially_involved_surgeons),
        '# possible patients': len(possible_patients),
        '# possible patterns': len(possible_patterns),
        '# possible surgeons': len(possible_surgeons),

        'mu': dual_variables['mu'][block.id],

        'block load': block_load,
        'block load fractional': block_load_fractional,

        'clinical priority average': clinical_priority,
        'clinical priority fractional average': clinical_priority_fractional,
        'clinical priority possible average': clinical_priority_possible,

        'waiting days average': waiting_days,
        'waiting days fractional average': waiting_days_fractional,
        'waiting days possible average': waiting_days_possible,

        'surgery time average': surgery_time,
        'surgery time fractional average': surgery_time_fractional,
        'surgery time possible average': surgery_time_possible,

        'setup time average': setup_time,
        'setup time fractional average': setup_time_fractional,
        'setup time possible average': setup_time_possible,

        'block schedule is integral': is_block_integral,
        'patient branching constraints 0': len(patient_branching_constraints_0),
        'patient branching constraints 1': len(patient_branching_constraints_1),
        'surgeon branching constraints 0': len(surgeon_branching_constraints_0),
        'surgeon branching constraints 1': len(surgeon_branching_constraints_1),

        'SP objective value': sp_objective_value,
        'MP impact': mp_impact
    }

    # append histogram features to the dictionary with features
    histograms = [
        (clinical_priority_hist, 'clinical priority histogram'),
        (clinical_priority_fractional_hist, 'clinical priority fractional histogram',),
        (clinical_priority_possible_hist, 'clinical priority possible histogram'),
        (waiting_days_hist, 'waiting days histogram'),
        (waiting_days_fractional_hist, 'waiting days fractional histogram'),
        (waiting_days_possible_hist, 'waiting days possible histogram'),
        (surgery_time_hist, 'surgery time histogram'),
        (surgery_time_fractional_hist, 'surgery time fractional histogram'),
        (surgery_time_possible_hist, 'surgery time possible histogram'),
        (setup_time_hist, 'setup time histogram'),
        (setup_time_fractional_hist, 'setup time fractional histogram'),
        (setup_time_possible_hist, 'setup time possible histogram')
    ]

    for hist, key in histograms:
        new_dict = {}
        for idx, el in enumerate(hist):
            new_dict[f'{key} {idx}'] = el
        subproblem_features.update(new_dict)

    return subproblem_features


def record_subproblems(master_problem):

    features = []

    # load the original MP model and solve it (preparation for measuring the impact of subproblem)
    MP_before = prepare_bap.MasterProblemPrimal(copy.deepcopy(master_problem.instance), copy.deepcopy(master_problem.branching_constraints), copy.deepcopy(master_problem.idx))
    MP_before.optimize_model(max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), integer=False)
    objective_before = MP_before.objective_value

    # solve subproblem for (block, room, day)...
    for block in master_problem.instance.blocks:
        room, day = master_problem.instance.get_room(block.room), block.day

        # solve the subproblem for the pattern
        subproblem = prepare_bap.Subproblem(master_problem.instance)
        subproblem.optimize_model(max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), master_problem.dual_variables, master_problem.branching_constraints, block, day, room)

        # if the timelimit is reached, kill the procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            return

        # measure impact on the MP
        MP_impact = 0
        if subproblem.objective_value > 0:

            # crete Pattern corresponding to solved subproblem
            improving_item_patients = [master_problem.instance.get_patient(idx) for idx, item in enumerate(subproblem.variables['x']) if round(item, 2) == 1]
            improving_item_surgeons = [master_problem.instance.get_surgeon(idx) for idx, item in enumerate(subproblem.variables['n']) if round(item, 2) == 1]
            improving_pattern = prepare_bap.Pattern(max([pattern.id for pattern in master_problem.instance.patterns]) + 1 if master_problem.instance.patterns else 0,
                                                    block, improving_item_patients, improving_item_surgeons, subproblem.variables['o'], subproblem.variables['z'],
                                                    master_problem.instance.n_blocks, master_problem.instance.n_patients, master_problem.instance.n_surgeons)

            # optimize the model with new pattern
            MP_after = prepare_bap.MasterProblemPrimal(copy.deepcopy(master_problem.instance), copy.deepcopy(master_problem.branching_constraints), copy.deepcopy(master_problem.idx))
            MP_after.instance.patterns.append(improving_pattern)
            MP_after.optimize_model(max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), integer=False)
            objective_after = MP_after.objective_value

            # compute the impact on the master problem
            try:
                MP_impact = abs(objective_after - objective_before)
            except:
                continue

        # write down subproblem features
        subproblem_features = record_subproblem(master_problem, block, day, subproblem.objective_value, MP_impact)
        features.append(subproblem_features)

    if params.type_of_model == 'rank sp':

        # sort subproblems according to the SP objective value
        features = sorted(features, key=lambda x: x['SP objective value'])

        # write down the features together with the rank and batch index
        i = 1
        for idx, f in enumerate(features):
            f['batch index'] = params.batch_index
            if f['SP objective value'] == 0:
                f['SP rank'] = 1
                f['SP rank binary'] = 0
            else:
                if idx > 0 and features[idx-1]['SP objective value'] != f['SP objective value']:
                    i += 1
                f['SP rank'] = i
                f['SP rank binary'] = 1
            params.analysis.subproblem_features.append(f)
        params.batch_index += 1

    elif params.type_of_model == 'rank mp':

        # sort subproblems according to the MP impact
        features = sorted(features, key=lambda x: x['MP impact'])

        # write down the features together with the rank and batch index
        i = 1
        for idx, f in enumerate(features):
            f['batch index'] = params.batch_index
            if f['MP impact'] == 0:
                f['MP rank'] = 1
                f['MP rank binary'] = 0
            else:
                if idx > 0 and features[idx-1]['MP impact'] != f['MP impact']:
                    i += 1
                f['MP rank'] = i
                f['MP rank binary'] = 1
            params.analysis.subproblem_features.append(f)
        params.batch_index += 1

    elif params.type_of_model == 'clf':
        for f in features:
            params.analysis.subproblem_features.append(f)
    else:
        print('Warning: No model type defined!')
        exit()


def rank_subproblems(master_problem):
    """
    This function extracts features of a subproblem for each block in the current state (branching constrains, primal and
    dual variables) and then rank them using ML model with respect to their probability of having positive reduced cost.
    Objects are then sorted and returned (first object is the block with the highest probability of having positive reduced cost).

    :param master_problem: master problem instance
    :return: sorted block objects
    """

    instance, dual_variables, branching_constraints = master_problem.instance, master_problem.dual_variables, master_problem.branching_constraints

    def rank_subproblems_dummy():

        predictions = []

        # obtain randomized predictions for all the blocks
        for block in instance.blocks:
            room, day = instance.get_room(block.room), block.day

            # solve the subproblem for the pattern
            subproblem = prepare_bap.Subproblem(instance)
            subproblem.optimize_model(max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), dual_variables, branching_constraints, block, day, room)

            # if the SP is infeasible, continue with different subproblem
            if subproblem.model.status != g.GRB.OPTIMAL:
                prediction = 0
            else:
                prediction = 1 if subproblem.objective_value > 0 else 0

            predictions.append(prediction)

        return predictions

    # record subproblems' features
    start_time = time.time()
    inputs = []
    for block in instance.blocks:
        room, day = instance.get_room(block.room), block.day
        input_features = record_subproblem(master_problem, block, day, 0, 0)
        input_values = [value for key, value in input_features.items() if key not in ['SP objective value', 'MP impact']]
        inputs.append(input_values)
    params.analysis.ml_record_time += time.time() - start_time

    start_time = time.time()
    # rank subproblems using simpler classifier (random forest, XGBoost, ...)
    if params.type_of_model == 'clf':
        prediction = params.model.predict(np.array(inputs))
        random_0_blocks = np.where(prediction <= 0.5)[0]
        random_1_blocks = np.where(prediction > 0.5)[0]
        np.random.shuffle(random_0_blocks)
        np.random.shuffle(random_1_blocks)
        sorted_blocks = np.concatenate([random_1_blocks, random_0_blocks])

    # rank subproblems using LightGBM ranker
    if params.type_of_model in ['rank sp', 'rank mp']:
        prediction = params.model.predict(np.array(inputs))
        sorted_blocks = np.argsort(prediction)[::-1]

    # retrieve objects for the sorted blocks
    sorted_blocks_objects, sorted_blocks_predictions = [], []
    for block_id in sorted_blocks:
        sorted_blocks_objects.append(instance.get_block(block_id))
        sorted_blocks_predictions.append(prediction[block_id])

    params.analysis.ml_predict_time += time.time() - start_time

    # # compare predictions to ground truth
    # ground_truth = rank_subproblems_dummy()
    # print([int(p) for p in prediction])
    # print(ground_truth)
    # print(sorted_blocks)
    # print(np.nonzero(ground_truth)[0].tolist())

    return sorted_blocks_objects, sorted_blocks_predictions


def column_generation(master_problem):
    """
    This function performs the column generation algorithm.

    :param master_problem: current state of master problem to run the column generation algorithm on
    """

    # record average number of subproblems solved
    number_of_subproblems_array = []

    # while we generate improving patterns ...
    while True:

        mp_time_start = time.time()

        # if the timelimit is reached, kill the CG procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        # 1. pass improving patterns from last iteration subproblem to restricted MP, define it and solve it to optimize dual variables
        print("Optimizing master problem...")
        master_problem.optimize_model(timelimit=max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), integer=False)

        # update number of CG iterations and time value for analysis
        params.analysis.mp_cpu_time += time.time() - mp_time_start
        params.analysis.number_of_cg_iterations += 1

        # if the model is infeasible, cut off or time limited do not look for improving patterns
        if (master_problem.model.status != g.GRB.OPTIMAL and master_problem.model.status != g.GRB.TIME_LIMIT) or master_problem.model.SolCount == 0:
            print("Warning: MP model in CG is infeasible or cut off.")
            break

        # 2. pass variables from restricted MP to subproblem, define it and solve it to find improving patterns
        print("Generating improving patterns...")
        sp_time_start = time.time()
        sp_before = params.analysis.number_of_subproblems

        improving_patterns = generate_improving_pattern(master_problem)
        params.analysis.sp_cpu_time += time.time() - sp_time_start
        number_of_subproblems_array.append(params.analysis.number_of_subproblems - sp_before)

        if len(improving_patterns) == 0:
            print("No improving patterns, ending the column generation.")
            break

        # 3. if improving pattern (pattern with positive reduced cost) is found, add it to set of MP patterns and return to step 1, otherwise stop the process
        master_problem.instance.patterns.extend(improving_patterns)
        print("Adding following improving patterns to master problem:")
        for p in improving_patterns:
            p.print_pattern()
        print()

    # update average number of subproblems in this node
    params.graph.update_node(str(master_problem.idx), {'subproblems_avg': np.mean(np.asarray(number_of_subproblems_array))})


def branching(master_problem, master_problem_index, subject, horizon, indication, branching_subject, variable_copy=False):
    """
    This function performs the operation of branching. Subject for branching is selected together with the horizon for
    branching, proper branching objects and branch indication. New master problem is derived from the original with
    this new branch added and patterns removed according to the branching rule.

    :param master_problem: current state of master problem
    :param master_problem_index: index of the node of current state of master problem
    :param subject: index of the subject of branching (patient index / surgeon index)
    :param horizon: index of the horizon for branching (block / day)
    :param indication: 0 ... subject X is not assigned to horizon Y / 1 ... subject X is assigned to horizon Y
    :param branching_subject: subject to branch on ('patient' / 'surgeon' / 'aggregated surgeon' / 'extended aggregated surgeon')
    :param variable_copy: should also variables be copied to the new MP?
    :return: state of master problem after branching
    """

    print(f"ADDED BRANCHING CONSTRAINT: {branching_subject} {subject}{' not' if indication == 0 else ''} assigned to {'block' if branching_subject=='patient' else 'day'} {horizon}", flush=True)

    # copy master problem
    if not variable_copy:
        branched_master_problem = prepare_bap.MasterProblemPrimal(copy.deepcopy(master_problem.instance), copy.deepcopy(master_problem.branching_constraints), copy.deepcopy(master_problem_index))
    else:
        branched_master_problem = prepare_bap.MasterProblemPrimal(copy.deepcopy(master_problem.instance), copy.deepcopy(master_problem.branching_constraints), copy.deepcopy(master_problem_index), copy.deepcopy(master_problem.objective_value), copy.deepcopy(master_problem.primal_variables), copy.deepcopy(master_problem.dual_variables))

    # generate consequencing branching constraints:
    branching_rules = [(branching_subject, subject, horizon, indication)]

    # 1. surgeon not assigned to a day --> patients of that surgeon cannot be assigned to any block in that day
    if branching_subject == 'surgeon' and indication == 0:
        blocks = [block.id for block in branched_master_problem.instance.blocks if block.day == horizon]
        patients = [patient.id for patient in branched_master_problem.instance.patients if patient.surgeon == subject]
        for block in blocks:
            for patient in patients:
                if (patient, block, 0) not in branched_master_problem.branching_constraints['patient'] and (patient, block, 1) not in branched_master_problem.branching_constraints['patient']:
                    branching_rules.append(('patient', patient, block, 0))

    # 2. patient assigned to a block --> surgeon of that patient must be assigned to that day
    if branching_subject == 'patient' and indication == 1:
        surgeon = branched_master_problem.instance.get_patient(subject).surgeon
        day = branched_master_problem.instance.get_block(horizon).day
        if (surgeon, day, 1) not in branched_master_problem.branching_constraints['surgeon'] and (surgeon, day, 0) not in branched_master_problem.branching_constraints['surgeon']:
            branching_rules.append(('surgeon', surgeon, day, 1))

    for p_branching_subject, p_subject, p_horizon, p_indication in branching_rules[1:]:
        print(f"ADDED CONSEQUENCING BRANCHING CONSTRAINT: {p_branching_subject} {p_subject}{' not' if p_indication == 0 else ''} assigned to {'block' if p_branching_subject == 'patient' else 'day'} {p_horizon}", flush=True)

    # iterate over all the branching rules produced by the input one
    for p_branching_subject, p_subject, p_horizon, p_indication in branching_rules:

        # prepare a structure for storing patterns to be removed
        removed_patterns = []

        if p_branching_subject == 'patient':
            patient, block = p_subject, p_horizon

            # 'patient p is not assigned to block b' branch --> delete all patterns where p is in b
            if p_indication == 0:
                for p in branched_master_problem.instance.patterns:
                    if block == p.block.id and patient in p.patient_id:
                        removed_patterns.append(p)

            # 'patient p is assigned to block b' branch
            #       --> delete all patterns where p is not in b
            #       --> dummy pattern should be substituted with minimal one
            elif p_indication == 1:

                # remove current dummy pattern for block b and substitute it with minimal one
                dummy_pattern = branched_master_problem.instance.get_pattern(block)
                patient_object = branched_master_problem.instance.get_patient(patient)
                extended_patients, extended_surgeons = dummy_pattern.patients + [patient_object], dummy_pattern.surgeons + [branched_master_problem.instance.get_surgeon(patient_object.surgeon)]
                extended_overtime, extended_undertime = abs(min(0, 240 - sum([p.duration for p in extended_patients]))), max(0, 240 - sum([p.duration for p in extended_patients]))
                minimal_pattern = prepare_bap.Pattern(dummy_pattern.id, dummy_pattern.block, extended_patients, extended_surgeons, extended_overtime, extended_undertime, branched_master_problem.instance.n_blocks, branched_master_problem.instance.n_patients, branched_master_problem.instance.n_surgeons)
                branched_master_problem.instance.patterns.append(minimal_pattern)

                # delete all patterns where p is not in b
                for p in branched_master_problem.instance.patterns:
                    if (block != p.block.id and patient in p.patient_id) or (block == p.block.id and patient not in p.patient_id):
                        removed_patterns.append(p)

            # add constraint 'patient p is/is not assigned to block b' to the subproblem and remove patterns violating this condition from the master problem
            branched_master_problem.branching_constraints['patient'].append((patient, block, p_indication))

        elif p_branching_subject == 'surgeon':
            surgeon, day = p_subject, p_horizon

            # 'surgeon s is not assigned to day d' branch --> delete all patterns where s is in d
            if p_indication == 0:
                for p in branched_master_problem.instance.patterns:
                    if day == p.block.day and surgeon in p.surgeon_id:
                        removed_patterns.append(p)

            # add constraint 'surgeon s is/is not assigned to day d' to the master problem and remove patterns violating this condition from the master problem
            branched_master_problem.branching_constraints['surgeon'].append((surgeon, day, p_indication))

        # remove patterns violating added condition from the master problem
        for p in removed_patterns:
            p.print_pattern()
            branched_master_problem.instance.patterns.remove(p)

    # return edited master problem
    return branched_master_problem


def reduced_cost_fixing(master_problem, z_ilp_best):
    """
    This function performs the reduced cost fixing. This technique fixes nonbasic variables based on implications
    derived from optimality conditions. Its basic idea is that by the reduced cost of the variable we can
    determine whether this variable has improvement potential or not: when z_LP + reduced_cost_x > z_ILP_best, we can
    permanently set the variable to zero as because of fixing the variable to 1 leads to worse solution than
    the best integer solution we currently have (when performing the minimization problem).

    :param master_problem: current state of master problem
    :param z_ilp_best: objective value of the best integer solution we currently have
    :return: MP with fixed variables
    """

    # find all the variables to fix
    fixing_combinations = master_problem.find_fixing_combinations(z_ilp_best)
    print(f"Reduced cost fixing found following variables to fix: {fixing_combinations}")

    # update the MP with all the fixed variables
    master_problem_fixed = master_problem
    for surgeon, day in fixing_combinations:
        master_problem_fixed = branching(master_problem_fixed, master_problem_fixed.idx, surgeon, day, 0, 'surgeon', variable_copy=True)

    # add fixed variables to the graph
    if fixing_combinations:
        params.graph.update_node(str(master_problem_fixed.idx), {'RC_fixing': fixing_combinations, 'color': 'red'})

    # then continue with this updated MP instead of the initial one
    return master_problem_fixed


def branch_and_price(instance):
    """
    This function performs the branch and price algorithm.

    :param instance: input instance (declaration of patients, surgeons, blocks, rooms and days)
    :return: incumbent solution found by the algorithm
    """

    # prepare stack for branches (master problems), incumbent solution
    stack = []

    # generate initial set of feasible patterns according to provided instance and add them to the instance
    print("Generating initial feasible patterns...")
    instance.patterns = generate_initial_patterns(instance)
    for p in instance.patterns:
        p.print_pattern()

    # initialize root master problem and add it to the queue
    master_problem = prepare_bap.MasterProblemPrimal(instance, {'patient': [], 'surgeon': [], 'aggregated_surgeon': [], 'extended_aggregated_surgeon': []}, 0)
    params.graph.add_node(str(master_problem.idx), 0)
    stack.append(master_problem)

    # while there are unexplored nodes ...
    while len(stack) > 0:

        # >>> 1. pop a node from the stack, prepare the master problem

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        master_problem = stack.pop()
        master_problem = prepare_bap.MasterProblemPrimal(copy.deepcopy(master_problem.instance), copy.deepcopy(master_problem.branching_constraints), copy.deepcopy(master_problem.idx))

        print(f"\n{'=' * 150}",flush=True)
        print(utilities.Color.GREEN + '1. BRANCH SELECTION' + utilities.Color.END + '\n',flush=True)
        for constr in master_problem.branching_constraints['patient']:
            print(f"patient {constr[0]}{' not' if constr[2] == 0 else ''} assigned to block {constr[1]}",flush=True)
        for constr in master_problem.branching_constraints['surgeon']:
            print(f"surgeon {constr[0]}{' not' if constr[2] == 0 else ''} assigned to day {constr[1]}",flush=True)

        # >>> 2. run column generation; is the solution better than the incumbent solution?

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        print(f"\n{'=' * 150}",flush=True)
        print(utilities.Color.GREEN + '2. COLUMN GENERATION' + utilities.Color.END + '\n',flush=True)

        start_time, number_of_patterns_before = time.time(), len(master_problem.instance.patterns)

        # if the current node is deeper than a threshold, instead of CG run initial ILP with branching constraints, fathom the current node and move to the next one
        if params.graph.nodes[str(master_problem.idx)]['depth'] > params.threshold_depth:
            print(f"Node has depth which is > {params.threshold_depth}. ILP is solved instead of CG.",flush=True)

            # solve ILP located in MP object
            master_problem.optimize_ilp(timelimit=max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), bound=params.solution.objective_value)

            # make a note that ILP was performed in this node
            params.graph.update_node(str(master_problem.idx), {'method': 'ILP'})

            # if the ILP is infeasible or cut off because of the objective value, solution cannot be better, continue with next node
            if (master_problem.model.status != g.GRB.OPTIMAL and master_problem.model.status != g.GRB.TIME_LIMIT) or master_problem.model.SolCount == 0:
                print("ILP model is infeasible or cut off, solution cannot be better, fathoming current node and moving to the next one.",flush=True)
                continue

            # check if the solution is better than optimal solution - if so, update it and continue with next node
            if master_problem.objective_value < params.solution.objective_value:
                print(f"Solution of ILP ({master_problem.objective_value}) is better than the incumbent solution ({params.solution.objective_value}).",flush=True)
                print("No fractional variable in the primal master problem revealed, solution is integer, updating the incumbent solution, fathoming current node and moving to the next one.",flush=True)

                # update the node and incumbent solution
                params.graph.update_node(str(master_problem.idx), {
                    'integer_obj_val': str(master_problem.objective_value),
                    'incumbent_solution': params.solution.objective_value,
                    'cpu_time': str(round(time.time() - start_time, 3)),
                    'total_patterns': str(number_of_patterns_before),
                    'added_patterns': str(len(master_problem.instance.patterns) - number_of_patterns_before)
                })
                ('original', master_problem.idx, master_problem.objective_value, master_problem.optimality_gap, master_problem.primal_variables, master_problem.instance.patterns, time.time() - params.analysis.time_start)
            # otherwise, don't update the optimal solution and directly move to next node
            else:
                print(f"Solution of ILP ({master_problem.objective_value}) is not better than the incumbent solution ({params.solution.objective_value}), fathoming current node and moving to the next one.",flush=True)
            continue
        # otherwise, run normally CG
        else:
            column_generation(master_problem)

            # if the column generation is not optimal, fathom the current node and move to the next one
            if (master_problem.model.status != g.GRB.OPTIMAL and master_problem.model.status != g.GRB.TIME_LIMIT) or master_problem.model.SolCount == 0:
                print(f"Solution of master problem is infeasible or cut off. Fathoming current node and moving to the next one.",flush=True)
                continue

            # update the node
            params.graph.update_node(str(master_problem.idx), {
                'obj_val': str(master_problem.objective_value),
                'incumbent_solution': params.solution.objective_value,
                'cpu_time': str(round(time.time() - start_time, 3)),
                'total_patterns': str(number_of_patterns_before),
                'added_patterns': str(len(master_problem.instance.patterns) - number_of_patterns_before)
            })

        # >>>>>>> 2. a) YES, move to 3.

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        if master_problem.objective_value < params.solution.objective_value:
            print(f"Solution of master problem ({master_problem.objective_value}) is better than the incumbent solution ({params.solution.objective_value}). Continuing with branching.",flush=True)

        # >>>>>> 2. b) NO, solution is not promising, fathom the current node and move to the next node
        else:
            print(f"Solution of master problem ({master_problem.objective_value}) is not better than the incumbent solution ({params.solution.objective_value}). Fathoming current node and moving to the next one.",flush=True)
            continue

        # perform reduced cost fixing here
        master_problem = reduced_cost_fixing(master_problem, params.solution.objective_value)

        # >>> 3. is the solution of primal MP fractional for some patient, surgeon or aggregated surgeon?

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        is_fractional_patient, is_fractional_surgeon = master_problem.check_fractionality()
        print(f"\n{'=' * 150}",flush=True)
        print(utilities.Color.GREEN + '3. BRANCHING' + utilities.Color.END + '\n',flush=True)

        # >>>>>> 3. a) YES, move to 4
        if is_fractional_patient or is_fractional_surgeon:
            print("Solution of the primal master problem is fractional, moving to the master heuristic.",flush=True)

        # >>>>>> 3. b) NO, update incumbent solution, fathom the current node and move to the next node
        else:
            print("No fractional variable in the primal master problem revealed, solution is integer, updating the incumbent solution, fathoming current node and moving to the next one.",flush=True)
            params.solution.update('master_problem', master_problem.idx, master_problem.objective_value, master_problem.optimality_gap, master_problem.primal_variables, master_problem.instance.patterns, time.time() - params.analysis.time_start)
            continue

        # >>> 4. run master heuristic; is the solution feasible and not cut off because of the bound?

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        mh_time_start = time.time()
        master_problem.optimize_model(timelimit=max(TIMELIMIT - (time.time() - params.analysis.time_start), 0), integer=True, bound=params.solution.objective_value)
        params.analysis.mh_cpu_time += time.time() - mh_time_start

        # >>>>>> 4. b) NO, move to 5
        if (master_problem.model.status != g.GRB.OPTIMAL and master_problem.model.status != g.GRB.TIME_LIMIT) or master_problem.model.SolCount == 0:
            print("Solution of master heuristic is infeasible or cut off. Moving to the branching.",flush=True)

        # >>>>>> 4. b) YES, if the solution is better than the solution of incumbent solution, update it and move to 5
        else:
            print("Solution of master heuristic is feasible, comparing with the incumbent solution.",flush=True)
            params.graph.update_node(str(master_problem.idx), {'integer_obj_val': str(master_problem.heuristic_objective_value)})

            if master_problem.heuristic_objective_value < params.solution.objective_value:
                print(f"Solution of master heuristic ({master_problem.heuristic_objective_value}) is better than the incumbent solution ({params.solution.objective_value}). Updating the incumbent solution and moving to the branching.",flush=True)
                params.solution.update('master_problem', master_problem.idx, master_problem.heuristic_objective_value, master_problem.optimality_gap, master_problem.heuristic_variables, master_problem.instance.patterns, time.time() - params.analysis.time_start)
                params.analysis.incumbent_solution_found = time.time()
            else:
                print(f"Solution of master heuristic ({master_problem.heuristic_objective_value}) is not better than the incumbent solution ({params.solution.objective_value}). Moving to the branching.",flush=True)

        # >>> 5. branch to fixate one fractional variable

        # if the timelimit is reached, kill the BAP procedure
        if time.time() - params.analysis.time_start > TIMELIMIT:
            break

        # fixate fractional surgeon
        if is_fractional_surgeon:
            fractional_surgeon, fractional_day = master_problem.find_fractional_variable(selection_type='random order', variable_type='surgeon')
            print(f"Branching: selecting fractional combination of surgeon {fractional_surgeon} and day {fractional_day}.\n",flush=True)

            # first branch - surgeon s is not assigned to day d
            stack.append(branching(master_problem, params.graph.current_index, fractional_surgeon, fractional_day, 0, 'surgeon'))
            params.graph.add_edge(str(master_problem.idx), str(params.graph.current_index), f'surgeon {fractional_surgeon} not assigned to day {fractional_day}')
            params.graph.add_node(str(params.graph.current_index), params.graph.nodes[str(master_problem.idx)]['depth'] + 1)

            # second branch - surgeon s is assigned to day d
            stack.append(branching(master_problem, params.graph.current_index, fractional_surgeon, fractional_day, 1, 'surgeon'))
            params.graph.add_edge(str(master_problem.idx), str(params.graph.current_index), f'surgeon {fractional_surgeon} assigned to day {fractional_day}')
            params.graph.add_node(str(params.graph.current_index), params.graph.nodes[str(master_problem.idx)]['depth'] + 1)

            continue

        # fixate fractional patient
        elif is_fractional_patient:
            fractional_patient, fractional_block = master_problem.find_fractional_variable(selection_type='random order', variable_type='patient')
            print(f"Branching: selecting fractional combination of patient {fractional_patient} and block {fractional_block}.\n",flush=True)

            # first branch - patient p is not assigned to block b
            stack.append(branching(master_problem, params.graph.current_index, fractional_patient, fractional_block, 0, 'patient'))
            params.graph.add_edge(str(master_problem.idx), str(params.graph.current_index), f'patient {fractional_patient} not assigned to block {fractional_block}')
            params.graph.add_node(str(params.graph.current_index), params.graph.nodes[str(master_problem.idx)]['depth'] + 1)

            # second branch - patient p is assigned to block b
            stack.append(branching(master_problem, params.graph.current_index, fractional_patient, fractional_block, 1, 'patient'))
            params.graph.add_edge(str(master_problem.idx), str(params.graph.current_index), f'patient {fractional_patient} assigned to block {fractional_block}')
            params.graph.add_node(str(params.graph.current_index), params.graph.nodes[str(master_problem.idx)]['depth'] + 1)

    # store number of nodes value for analysis
    params.analysis.number_of_nodes = len(params.graph.nodes)

    # store maximal depth of the tree
    params.analysis.find_maximum_depth(params.graph.nodes)

    # colour the node where optimal solution was found
    params.graph.update_node(str(params.solution.mp_index), {'color': 'yellow'})

    print("Returning the best solution...",flush=True)


def main(path, arguments):
    """
    Main function to be called.

    :return: analysis and graph object
    """

    # random seed initialization to reproduce same results in every run
    manualSeed = 1
    np.random.seed(manualSeed)
    random.seed(manualSeed)
    torch.manual_seed(manualSeed)

    # reset metrics
    params.analysis.reset()
    params.solution.reset()
    params.graph.reset()

    # prepare instance, load parameters and start the timer
    instance = prepare_instance.main(path)
    params.load_arguments(arguments)
    params.analysis.time_start = time.time()

    # run the branch and price algorithm
    branch_and_price(instance)
    params.analysis.total_cpu_time = time.time() - params.analysis.time_start

    # print solution and analysis
    params.solution.print(instance)
    params.analysis.print()

    return params.analysis, params.solution, params.graph


if __name__ == '__main__':

    # parse arguments
    input_arguments = utilities.parse_arguments()
    file_arguments = utilities.parse_arguments_from_file()

    main(input_arguments.instance, file_arguments)
