"""
This file serves as a script for training and validation of specified random forest or XGBoost model using Scikit Learn.
"""

import numpy as np
import pandas as pd
import random
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import seaborn as sns
import matplotlib.pyplot as plt
import pickle

import utilities

# random seed initialization to reproduce same results
manualSeed = 1
np.random.seed(manualSeed)


def create_dataset(path):
    """
    This function loads data in the form of CSV, preprocess them and return train and test
    data and labels in the form accepted by the Scikit Learn models.

    :param path: path to the data CSV file
    :return: train data, test data, train labels, test labels, name of features
    """

    # prepare features to be operated
    drop_features = ['SP objective value', 'MP impact', 'batch index', 'SP rank', 'SP rank binary']
    target_feature = 'SP rank binary'

    # load CSV as DataFrame object
    df = pd.read_csv(path, index_col=False, low_memory=False)

    # prepare the dataset
    X = df.copy().drop(drop_features, axis=1)
    print(len(X.columns))
    print(X.columns)
    X = X.to_numpy()

    # prepare the labels
    y = df.copy()[target_feature].to_numpy()
    y[np.where(y > 0.5)] = 1
    # y = df.copy()['SP objective value'].to_numpy()
    # y[np.where(y > 0.5)] = 1

    print(f"There are {np.count_nonzero(y == 0)} samples in class 0 and {np.count_nonzero(y == 1)} samples in class 1.")

    # stratify dataset
    indices_0, indices_1 = np.where(y == 0)[0], np.where(y == 1)[0]
    X = X[np.concatenate([indices_0[random.sample(range(0, indices_0.shape[0]), indices_1.shape[0])], indices_1])]
    y = y[np.concatenate([indices_0[random.sample(range(0, indices_0.shape[0]), indices_1.shape[0])], indices_1])]
    print(f"There are {np.count_nonzero(y==0)} samples in class 0 and {np.count_nonzero(y==1)} samples in class 1.")

    # split the data to train and validation part
    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    return X_train, X_test, Y_train, Y_test, df.columns


def process_of_training(data_path, model_path=None):
    """
    This function is a main function for training a model.

    :param data_path: path to the data
    :param model_path: path to the model
    """

    # load training data and labels
    print(f"Loading the data {data_path}...")
    X_train, X_test, Y_train, Y_test, feature_names = create_dataset(data_path)

    # create classifer object and train it
    clf = RandomForestClassifier(n_jobs=-1, max_depth=15)
    clf = clf.fit(X_train, Y_train)

    # get the predictions for the test dataset
    Y_pred = clf.predict(X_test)

    # compute model performance
    print("Accuracy: ", metrics.accuracy_score(Y_test, Y_pred))
    print("Precision: ", metrics.precision_score(Y_test, Y_pred))
    print("Recall: ", metrics.recall_score(Y_test, Y_pred))

    # plot confusion matrix
    cf_matrix = metrics.confusion_matrix(Y_test, Y_pred)
    plt.figure(figsize=(10, 10))
    ax = sns.heatmap(cf_matrix, annot=True, cmap='Blues', fmt='.8g')
    ax.xaxis.set_ticklabels(['False', 'True'])
    ax.yaxis.set_ticklabels(['False', 'True'])
    ax.set_xlabel('Prediction')
    ax.set_ylabel('Ground Truth')
    plt.show()

    # save the model
    if model_path:
        pickle.dump(clf, open(model_path, 'wb'))
    else:
        return clf


if __name__ == '__main__':

    # parse arguments
    arguments = utilities.parse_arguments()

    # train a model
    timestamp = datetime.now().strftime("%d-%m-%Y-%H-%M")
    path_to_model = None#f'models/rf_{timestamp}.sav'
    process_of_training(arguments.instance, path_to_model)

