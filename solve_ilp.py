"""
This file serves as a main script for performing the ILP optimization of
declared model using Gurobi solver.
"""

import gurobipy as g
import numpy as np
import time
import prepare_instance, utilities


# prepare global structure for analysis and time limit
ANALYSIS = utilities.Analysis()
SOLUTION = utilities.Solution()
TIMELIMIT = 4 * 60 * 60


def ilp(instance):
    """
    This function describes a deterministic mathematical formulation of the problem using
    Gurobi model and then solves it to the optimality.

    :param instance: instance on which the OR scheduling task is performed
    :return: optimized model, optimal variables' values
    """

    # create empty model
    model = g.Model()
    model.setParam(g.GRB.Param.TimeLimit, TIMELIMIT)
    model.setParam(g.GRB.Param.Presolve, 0)
    model.setParam(g.GRB.Param.MIPGap, 1e-6)
    model.setParam(g.GRB.Param.IntFeasTol, 1e-6)

    # create variables
    x = model.addVars(instance.n_patients, instance.n_blocks, vtype=g.GRB.BINARY, name="x")
    o = model.addVars(instance.n_blocks, lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="o")
    z = model.addVars(instance.n_blocks, lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="z")
    n = model.addVars(instance.n_surgeons, instance.n_days, vtype=g.GRB.BINARY, name="n")
    y = model.addVars(instance.n_patients+1, instance.n_patients+1, instance.n_blocks, vtype=g.GRB.BINARY, name="y")
    e = model.addVars(instance.n_blocks, vtype=g.GRB.BINARY, name="e")

    # add constraints
    for patient in instance.patients:
        model.addConstr(g.LinExpr((1, x[patient.id, block.id]) for block in instance.blocks) <= 1, name=f"constraint_1_p{patient.id}")
    for block in instance.blocks:
        model.addConstr(g.LinExpr((patient.duration, x[patient.id, block.id]) for patient in instance.patients) + g.LinExpr((instance.sequence_duration[patient_1.group][patient_2.group], y[patient_1.id, patient_2.id, block.id]) for patient_1 in instance.patients for patient_2 in instance.patients if patient_2.id != patient_1.id) - block.capacity == o[block.id] - z[block.id], name=f"constraint_2_b{block.id}")
    model.addConstrs((o[block.id] <= block.overtime for block in instance.blocks), name="constraint_3")
    for room in instance.rooms:
        for day in instance.days:
            model.addConstr(g.LinExpr((1, o[block.id]) for block in instance.blocks if block.day == day) <= room.overtime, name=f"constraint_4_r{room.id}_d{day}")
    for surgeon in instance.surgeons:
        for day in instance.days:
            model.addConstr(g.LinExpr((1, x[patient.id, block.id]) for block in instance.blocks if block.day == day for patient in instance.patients if patient.surgeon == surgeon.id) <= instance.max_patients_surgeon_day[surgeon.id] * n[surgeon.id, day], name=f"constraint_5_s{surgeon.id}_d{day}")
    model.addConstrs((x[patient.id, block.id] == 0 for block in instance.blocks for patient in instance.patients if sum([1 if patient.surgeon == surgeon.id and block.id in surgeon.blocks else 0 for surgeon in instance.surgeons]) == 0), name="constraint_6")
    model.addConstrs((x[patient.id, block.id] == 0 for patient in instance.patients for block in instance.blocks if patient.release > block.day), name="constraint_7")

    for patient in instance.patients:
        for block in instance.blocks:
            model.addConstr(x[patient.id, block.id] == g.LinExpr((1, y[patient.id, patient_next.id, block.id]) for patient_next in instance.patients if patient_next.id != patient.id) + y[patient.id, instance.n_patients, block.id])
            model.addConstr(x[patient.id, block.id] == g.LinExpr((1, y[patient_prev.id, patient.id, block.id]) for patient_prev in instance.patients if patient_prev.id != patient.id) + y[instance.n_patients, patient.id, block.id])
            model.addConstr(x[patient.id, block.id] <= e[block.id])
    for block in instance.blocks:
        model.addConstr(g.LinExpr((1, y[patient.id, instance.n_patients, block.id]) for patient in instance.patients) == e[block.id])
        model.addConstr(g.LinExpr((1, y[instance.n_patients, patient.id, block.id]) for patient in instance.patients) == e[block.id])
        model.addConstr(e[block.id] <= g.LinExpr((1, x[patient.id, block.id]) for patient in instance.patients))

    # set objective
    obj1 = g.LinExpr((((day - patient.release)) * patient.priority, x[patient.id, block.id]) for patient in instance.patients for day in instance.days for block in instance.blocks if block.day == day)
    obj5 = g.LinExpr((1, x[patient.id, block.id]) for patient in instance.patients for block in instance.blocks)
    obj6 = g.LinExpr((1, n[surgeon.id, day]) for surgeon in instance.surgeons for day in instance.days)
    obj7 = g.quicksum(o[block.id] * o[block.id] for block in instance.blocks)
    obj8 = g.quicksum(z[block.id] * z[block.id] for block in instance.blocks)
    objective = instance.weights['m1'] * obj1 - instance.weights['m5'] * obj5 + instance.weights['m6'] * obj6 + instance.weights['m7'] * obj7 + instance.weights['m7'] * obj8
    model.setObjective(objective, sense=g.GRB.MINIMIZE)

    # optimize the model
    model.optimize()

    # store all the model variables
    variables = {
        'x': utilities.convert_dictionary_to_array(model.getAttr('X', x), np.zeros((instance.n_patients, instance.n_blocks))),
        'o': model.getAttr('X', o.values()),
        'z': model.getAttr('X', z.values()),
        'n': utilities.convert_dictionary_to_array(model.getAttr('X', n), np.zeros((instance.n_surgeons, instance.n_days)))
    }

    return model, variables


def main(path):
    """
    Main function to be called.

    :param path: path to the input JSON file with the instance
    :return: recorded analysis
    """

    # reset analysis
    ANALYSIS.reset()

    # prepare instance
    instance = prepare_instance.main(path)

    # run ILP
    total_time_start = time.time()
    model_ilp, variables_ilp = ilp(instance)

    # store total CPU time value, time of finding incumbent solution and incumbent solution objective value for analysis
    ANALYSIS.total_cpu_time = time.time() - total_time_start
    SOLUTION.objective_value = model_ilp.objVal
    SOLUTION.optimality_gap = model_ilp.MIPGap

    # if the ILP was solved in timelimit, print the solution
    print('\n' + utilities.Color.PURPLE + f"{'#' * 50}")
    print('# SOLUTION')
    print(f"{'#' * 50}" + utilities.Color.END + '\n')
    for b in instance.blocks:
        patients = [instance.get_patient(idx) for idx, item in enumerate(variables_ilp['x'][:, b.id]) if item == 1]
        print(f"| BLOCK: {b.id} | PATIENTS: {[patient.id for patient in patients]} | SURGEONS: {list(set([patient.surgeon for patient in patients]))} | OVERTIME: {variables_ilp['o'][b.id]} | UNDERTIME: {variables_ilp['z'][b.id]}")
    print(f"OBJECTIVE VALUE: {model_ilp.objVal}")
    print(f"OPTIMALITY GAP: {model_ilp.MIPGap}")
    print(f"Total CPU time: {ANALYSIS.total_cpu_time} seconds")

    return ANALYSIS, SOLUTION


if __name__ == '__main__':

    # parse arguments
    arguments = utilities.parse_arguments()

    # solve ILP
    main(arguments.instance)

