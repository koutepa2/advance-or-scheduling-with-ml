"""
This file serves as a script for training and validation of LambdaRank model using LightGBM.
"""

import numpy as np
import pandas as pd
from datetime import datetime
import pickle
import lightgbm as lgb
import random
import shap
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

import utilities

# random seed initialization to reproduce same results
manualSeed = 1
np.random.seed(manualSeed)

matplotlib.pyplot.rcParams.update({
    "text.usetex": True,
    'font.size': 22,
    "font.family": "serif",
    "axes.grid" : True,
    "grid.color": "#e6e6e6",
    "axes.axisbelow": True
})


def create_dataset(path):
    """
    This function loads data in the form of CSV, preprocess them and return train and test
    data and labels in the form accepted by the Scikit Learn models.

    :param path: path to the data CSV file
    :return: train data, test data, train labels, test labels, name of features
    """

    # prepare features to be operated
    drop_features = ['SP objective value', 'MP impact', 'batch index', 'SP rank', 'SP rank binary']
    target_feature = 'SP rank'

    # load CSV as DataFrame object
    df = pd.read_csv(path, index_col=False, low_memory=False)

    # split dataset to batches, shuffle records inside the batches and shuffle the overall order of batches
    batch_dataframes = [d for _, d in df.groupby('batch index')]
    shuffled_batch_dataframes = [batch.sample(frac=1).reset_index(drop=True) for batch in batch_dataframes]
    random.shuffle(shuffled_batch_dataframes)

    # merge batches to train and validation part and record batch query sizes
    train_size = int(len(shuffled_batch_dataframes) * 0.8)
    train_dataframes, test_dataframes = shuffled_batch_dataframes[:train_size], shuffled_batch_dataframes[train_size:]
    query_train, query_test = [len(batch) for batch in train_dataframes], [len(batch) for batch in test_dataframes]
    train_dataset, test_dataset = pd.concat(train_dataframes, ignore_index=True), pd.concat(test_dataframes, ignore_index=True)

    # split data to features and labels and drop not wanted features
    X_train, X_test = train_dataset.drop(drop_features, axis=1), test_dataset.drop(drop_features, axis=1)
    Y_train, Y_test = train_dataset[target_feature].to_numpy(), test_dataset[target_feature].to_numpy()
    print(len(X_train.columns))
    print(X_train.columns)

    return X_train, X_test, Y_train, Y_test, query_train, query_test


def generate_shap_plots(ranker, X_train, feature_names):
    """
    Generates SHAP plots for a pre-trained LightGBM model.

    :param ranker: A trained LightGBM model
    :param X_train: The training data used to fit the model
    :param feature_names: list of feature names
    """

    X_train = X_train[:4000]

    feature_names = [f.replace('#', '\#') for f in feature_names]
    X_train.columns = [f.replace('#', '\#') for f in X_train.columns]
    explainer = shap.Explainer(ranker, X_train, feature_names=feature_names)
    shap_values = explainer(X_train, check_additivity=False)

    default_pos_color, default_neg_color = "#ff0051", "#008bfb"
    negative_color, positive_color = "#92C5DE", "#B2182B"
    newCmap = LinearSegmentedColormap.from_list("", [negative_color, positive_color])

    plt.figure()
    shap.summary_plot(shap_values, feature_names=feature_names, plot_type='dot', cmap=newCmap, show=False,  max_display=15)
    w, _ = plt.gcf().get_size_inches()
    plt.gcf().set_size_inches(w, w * 3 / 4)
    plt.tight_layout()
    plt.savefig("shap_dot.pdf", format="pdf", dpi=1000, bbox_inches="tight")
    plt.show()

    for i in range(10):
        # Plot bar plot
        shap.plots.bar(shap_values[i], show=False)
        # Change the colormap of the artists
        for fc in plt.gcf().get_children():
            # Ignore last Rectangle
            for fcc in fc.get_children()[:-1]:
                if (isinstance(fcc, matplotlib.patches.Rectangle)):
                    if (matplotlib.colors.to_hex(fcc.get_facecolor()) == default_pos_color):
                        fcc.set_facecolor(positive_color)
                    elif (matplotlib.colors.to_hex(fcc.get_facecolor()) == default_neg_color):
                        fcc.set_color(negative_color)
                elif (isinstance(fcc, plt.Text)):
                    if (matplotlib.colors.to_hex(fcc.get_color()) == default_pos_color):
                        fcc.set_color(positive_color)
                    elif (matplotlib.colors.to_hex(fcc.get_color()) == default_neg_color):
                        fcc.set_color(negative_color)
        w, _ = plt.gcf().get_size_inches()
        plt.gcf().set_size_inches(w, w * 3 / 4)
        plt.tight_layout()
        plt.savefig(f"shap_bar_{i}.pdf", format="pdf", dpi=1000, bbox_inches="tight")
        plt.show()



def process_of_training(data_path, model_path=None):
    """
    This function is a main function for training a model.

    :param data_path: path to the data
    :param model_path: path to the model
    """

    # load training data and labels
    print(f"Loading the data {data_path}...")
    X_train, X_test, Y_train, Y_test, query_train, query_test = create_dataset(data_path)

    # create ranker object and train it
    gbm = lgb.LGBMRanker(
        objective="lambdarank",
        metric="ndcg",
        num_iterations=3000
    )
    gbm.fit(
        X=X_train,
        y=Y_train,
        group=query_train,
        eval_set=[(X_test, Y_test)],
        eval_group=[query_test],
        eval_at=[1, 2, 5, 10]  # we want NDCG@k = while calculating NDCG, only top K relevance contributes to the final calculation
    )

    # # generate Shap plots
    # generate_shap_plots(gbm, X_train, list(X_train.columns))

    # # save the model
    # if model_path:
    #     pickle.dump(gbm, open(model_path, 'wb'))
    # else:
    #     return gbm

import time
if __name__ == '__main__':

    start = time.time()

    # parse arguments
    arguments = utilities.parse_arguments()

    # train a model
    timestamp = datetime.now().strftime("%d-%m-%Y-%H-%M")
    path_to_model = f'models/lightgbm_{timestamp}.sav'
    process_of_training(arguments.instance, path_to_model)

    print(time.time() - start)

