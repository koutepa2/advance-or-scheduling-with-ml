"""
This file serves as a preparation script for an input JSON file. It parses all information contained in the input
JSON file and converts it to the format of Instance class object which is then used for the optimization.

Apart from the parsing function, this file also declares all the class definitions for used entities - Instance,
Room, Block, Surgeon and Patient.
"""

import utilities


class Room:
    """
    A class used to represent one operating room (OR) from the given set of ORs.
    """
    def __init__(self, id, overtime):
        """
        :param id: ID of the OR
        :param overtime: maximum overtime allowed by the operating room
        """
        self.id = id
        self.overtime = overtime


class Block:
    """
    A class used to represent one block from the given set of blocks.
    """
    def __init__(self, id, overtime, capacity, day, room):
        """
        :param id: ID of the block
        :param overtime:  maximum overtime allowed by the block
        :param capacity: capacity of the block
        :param day: day to which the block belongs to
        :param room: room to which the block belongs to
        """
        self.id = id
        self.overtime = overtime
        self.capacity = capacity
        self.day = day
        self.room = room


class Surgeon:
    """
    A class used to represent one surgeon from the given set of surgeons.
    """
    def __init__(self, id, blocks):
        """
        :param id: ID of the surgeon
        :param blocks: set of blocks to which the surgeon belongs to
        """
        self.id = id
        self.blocks = blocks


class Patient:
    """
    A class used to represent one patient from the given set of patients.
    """
    def __init__(self, id, release, duration, priority, surgeon, group):
        """
        :param id: ID of the patient
        :param release: release time/date for the surgery of the patient
        :param duration: expected surgery duration of the patient
        :param priority: clinical priority coefficient of the patient
        :param surgeon: surgeon by which the patient needs to be treated
        :param group: patient group to which the patient belongs to
        """
        self.id = id
        self.release = release
        self.duration = duration
        self.priority = priority
        self.surgeon = surgeon
        self.group = group


class Instance:
    """
    A class used to represent the instance on which the OR scheduling task should be performed.
    """
    def __init__(self, max_blocks_surgeon_day, weights, sequence_duration):
        """
        :param max_blocks_surgeon_day: maximum number of blocks assigned to a surgeon in any day
        :param weights: dictionary of weights m_1,m_5,m_6,m_7 of term m_j in the objective function
        :param sequence_duration: matrix defining sequence dependent setup time
        """
        self.rooms, self.blocks, self.surgeons, self.patients, self.days = [], [], [], [], []
        self.n_rooms, self.n_blocks, self.n_surgeons, self.n_patients, self.n_days = 0, 0, 0, 0, 0
        self.max_blocks_surgeon_day = max_blocks_surgeon_day
        self.max_patients_surgeon_day = {}
        self.weights = weights
        self.sequence_duration = sequence_duration
        self.patterns = []

    def count_items(self):
        """
        This function counts all items of specific type in the instance (blocks, rooms, patients, ...).
        """
        self.n_rooms, self.n_blocks, self.n_surgeons, self.n_patients, self.n_days = len(self.rooms), len(self.blocks), len(self.surgeons), len(self.patients), len(self.days)
        self.max_patients_surgeon_day = {i: 0 for i in range(self.n_surgeons)}
        for p in self.patients:
            self.max_patients_surgeon_day[p.surgeon] += 1

    def get_room(self, id):
        """
        This function returns a room object with provided ID (if present in the instance).

        :param id: ID of the room object
        """
        return next((x for x in self.rooms if x.id == id), None)

    def get_block(self, id):
        """
        This function returns a block object with provided ID (if present in the instance).

        :param id: ID of the block object
        """
        return next((x for x in self.blocks if x.id == id), None)

    def get_surgeon(self, id):
        """
        This function returns a surgeon object with provided ID (if present in the instance).

        :param id: ID of the surgeon object
        """
        return next((x for x in self.surgeons if x.id == id), None)

    def get_patient(self, id):
        """
        This function returns a patient object with provided ID (if present in the instance).

        :param id: ID of the patient object
        """
        return next((x for x in self.patients if x.id == id), None)

    def get_pattern(self, id):
        """
        This function returns a pattern object with provided ID (if present in the instance).

        :param id: ID of the patient object
        """
        return next((x for x in self.patterns if x.id == id), None)

    def print_instance(self):
        """
        This function prints chosen instance in the format used by the scheduling algorithms.
        """

        print(f"\n{'-' * 150}\n")

        # print days and blocks
        for day in self.days:
            print(utilities.Color.RED + f"| DAY: {day} ", end='')
            for room in self.rooms:
                print(f"| ROOM: {room.id} (overtime: {room.overtime}) |" + utilities.Color.END)
                for block in self.blocks:
                    if block.room == room.id and block.day == day:
                        print(f"\t> BLOCK: {block.id} (overtime: {block.overtime}, capacity: {block.capacity})")
                print()

        # print surgeons
        for surgeon in self.surgeons:
            print(f"SURGEON: {surgeon.id} (blocks: {surgeon.blocks})")

        # print patients and their surgeries
        for patient in self.patients:
            print(f"PATIENT: {patient.id} (release: {patient.release}, duration: {patient.duration}, priority: {patient.priority}, surgeon: {patient.surgeon}), group: {patient.group}")

        print(f"\n{'-' * 150}\n")

    def print_patterns(self):
        """
        This function prints instance's patterns in the format used by the scheduling algorithms.
        """
        for pattern in self.patterns:
            print(f"| PATTERN: {pattern.id} | BLOCKS: {[pattern.block.id]} | PATIENTS: {pattern.patient_id} | SURGEONS: {pattern.surgeon_id} | OVERTIME: {pattern.overtime}")


def convert_json_to_instance(json):
    """
    This function converts the input JSON object to the instance format used by the scheduling algorithms.

    :param json: parsed JSON object
    """

    # prepare Instance object
    instance = Instance(json['max_blocks_surgeon_day'], json['weights'], json['sequence_duration'])

    # add all elements of the JSON file to the Instance object
    for key, item in enumerate(json['rooms']):
        instance.rooms.append(Room(key, item['overtime']))
    for key, item in enumerate(json['blocks']):
        instance.blocks.append(Block(key, item['overtime'], item['capacity'], item['day'], item['room']))
    for key, item in enumerate(json['surgeons']):
        instance.surgeons.append(Surgeon(key, item['blocks']))
    for key, item in enumerate(json['patients']):
        instance.patients.append(Patient(key, item['release'], item['duration'], item['priority'], item['surgeon'], item['group']))
    for day in range(json['number_of_days']):
        instance.days.append(day)

    return instance


def main(path):
    """
    Main function to be called.

    :param path: path to the input JSON file with the instance
    :return: prepared instance
    """

    # load and parse JSON
    json = utilities.parse_json(path)

    # prepare instance and print it
    instance = convert_json_to_instance(json)
    instance.count_items()
    instance.print_instance()

    # return the instance
    return instance


if __name__ == '__main__':

    # parse arguments
    arguments = utilities.parse_arguments()

    # prepare instance
    main(arguments.instance)
