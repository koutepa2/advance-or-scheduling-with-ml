"""
This file serves as the main script for the validation of the instances stored in some folder. It is the wrapper around all the parts described above that
runs for desired number of instances located in some folder and either record features of the subproblems or stores the results and measurements of the algorithm.
"""

import os
import csv
import pandas as pd
import utilities, solve_ilp, solve_bap


def create_csv(csv_path, header):
    """
    This function creates a CSV file with specified header.

    :param csv_path: path to the CSV file
    :param header: header of the CSV file
    """

    # if the CSV file already exists, delete it
    if os.path.exists(csv_path):
        os.remove(csv_path)

    # prepare a CSV file with writer and header to store the analysis of instances
    with open(csv_path, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        f.close()


def write_to_csv(csv_path, data):
    """
    This function writes new row of data to existing CSV file.

    :param csv_path: path to the CSV file
    :param data: row of data to be added to the file
    """
    with open(csv_path, 'a', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(data)
        f.close()


def calculate_stats(csv_path):
    """
    This function calculate stats (mean data) of provided CSV file.

    :param csv_path: path to the CSV file
    """

    # read the CSV file to the DataFrame
    df = pd.read_csv(csv_path)
    mean_data = []

    # compute means
    for column_name in df:
        column = df[column_name]
        if column.dtypes == 'object':
            mean_data.append('-')
        else:
            mean_data.append(column.mean())

    # write them to the CSV file
    write_to_csv(csv_path, mean_data)


def perform_analysis(folder, arguments, analysis_type):
    """
    This file is the main function for the analysis of a bunch of instances located in some folder.
    It takes every instance, performs the ILP and BAP, collects the required data (total CPU time,
    when was the incumbent solution found, ...) and writes them to a specified CSV file. After running
    through all the provided instances, it computes stats on that and stores it to the same CSV file.

    Two CSV files are created - one for the analysis of ILP (called 'ilp_analysis.csv') and one for
    the analysis of BAP (called 'bap_analysis.csv').

    It is also possible to provide folder with more subfolders with instances. In this case also
    two CSV files are created and located in the provided folder including all the instances from
    all the subfolders.

    :param folder: folder in which the analysis should be performed
    :param arguments: provided arguments
    :param analysis_type: type of analysis (ILP or BAP)
    """

    # create CSV file for writing results
    if analysis_type == 'ilp':
        csv_path = os.path.join(folder, 'ilp_analysis.csv')
    if analysis_type == 'bap':
        csv_path = os.path.join(folder, 'bap_analysis.csv')
    header = ['Instance name', 'Incumbent solution', 'Optimality gap', 'Incumbent solution found',
              'Total CPU time', 'MP CPU time', 'MH CPU time', 'SP CPU time', 'ML time', 'ML record time', 'ML predict time',
              'Number of nodes', 'Number of CG iterations', 'Number of subproblems', 'Maximum depth']
    create_csv(csv_path, header)

    # iterate over all the subfolders
    for folder_path, _, files in os.walk(folder):

        instances_path = [os.path.join(folder_path, f) for f in files if os.path.isfile(os.path.join(folder_path, f)) and f.endswith('.json')]

        # iterate over all the instances
        for idx, instance_path in enumerate(instances_path):

            print(utilities.Color.YELLOW + f"\n>>> ANALYSING INSTANCE {instance_path} <<<\n" + utilities.Color.END)

            # solve the ILP or BAP
            if analysis_type == 'ilp':
                analysis, solution = solve_ilp.main(instance_path)
            if analysis_type == 'bap':
                analysis, solution, graph = solve_bap.main(instance_path, arguments)
                graph.plot_tree(f"{instance_path.rsplit('.', 2)[0]}")

            # write down the analysis
            data = [instance_path, round(solution.objective_value, 3), round(solution.optimality_gap, 3), round(solution.solution_found, 3),
                        round(analysis.total_cpu_time, 3), round(analysis.mp_cpu_time, 3), round(analysis.mh_cpu_time, 3), round(analysis.sp_cpu_time, 3),
                        round(analysis.ml_overall_time, 3), round(analysis.ml_record_time, 3), round(analysis.ml_predict_time, 3),
                        analysis.number_of_nodes, analysis.number_of_cg_iterations, analysis.number_of_subproblems, analysis.maximum_depth]
            write_to_csv(csv_path, data)

    # calculate stats
    calculate_stats(csv_path)


def perform_recording(folder, arguments):
    """
    This file is the main function for the features recording of a bunch of instances located in some folder.
    It takes every instance, performs the BAP, collects the features of all the solved subproblems
    and writes them to a specified CSV file. One CSV file is created (called 'subproblem_features_recording.csv').

    It is also possible to provide folder with more subfolders with instances. In this case also
    one CSV file is created and located in the provided folder including all the instances from
    all the subfolders.

    :param folder: folder in which the analysis should be performed
    :param arguments: provided arguments
    """

    # prepare paths to the instances in provided folder and CSV file for recording subproblems
    csv_path = os.path.join(folder, 'subproblem_features_recording.csv')

    # iterate over all the subfolders
    for folder_path, _, files in os.walk(folder):

        instances_path = [os.path.join(folder_path, f) for f in files if os.path.isfile(os.path.join(folder_path, f)) and f.endswith('.json')]

        # iterate over all the instances
        for idx, instance_path in enumerate(instances_path):

            print(utilities.Color.YELLOW + f"\n>>> RECORDING INSTANCE {instance_path} <<<\n" + utilities.Color.END)

            # solve the BAP and write down the analysis
            analysis, solution, graph = solve_bap.main(instance_path, arguments)

            # prepare CSV file with header for recording subproblem attributes if analysing first instance
            if idx == 0:
                header = [key for key, value in analysis.subproblem_features[0].items()]
                create_csv(csv_path, header)

            # write down the subproblem features
            for s in analysis.subproblem_features:
                data = [value for key, value in s.items()]
                write_to_csv(csv_path, data)


def main(folder, arguments):
    """
    Main function to be called.
    """

    # perform the ILP analysis of instances in provided folder
    if arguments['type_of_validation'] == 'analyse ilp':
        perform_analysis(folder, arguments, 'ilp')

    # perform the BAP analysis of instances in provided folder
    if arguments['type_of_validation'] == 'analyse bap':
        perform_analysis(folder, arguments, 'bap')

    # perform the recording of instances in provided folder
    if arguments['type_of_validation'] == 'record':
        perform_recording(folder, arguments)


if __name__ == '__main__':

    # parse arguments
    input_arguments = utilities.parse_arguments()
    file_arguments = utilities.parse_arguments_from_file()

    # analyse instances
    main(input_arguments.folder, file_arguments)

