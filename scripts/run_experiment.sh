#!/bin/bash

# define folder
NAME="record"
FOLDER="/home/koutepa2/advance-or-scheduling-with-ml/instances/record_small"

# search through all the subfolders in the defined folder
for SUBDIR in $(find $FOLDER -mindepth 1 -maxdepth 1 -type d); do

	echo "Running validation for $SUBDIR"

	# run job for each of the subfolders
	sbatch --job-name="$NAME" --output="$SUBDIR/output.out" --error="$SUBDIR/output.err" scripts/run.sh "$SUBDIR"
	
done
