#!/bin/bash
#SBATCH --job-name=t1400_30h_throughput
#SBATCH --output=/home/suchap/dev/Prevedig/results/out/t1400_30h_throughput.out
#SBATCH --error=/home/suchap/dev/Prevedig/results/out/t1400_30h_throughput.err
#SBATCH --cpus-per-task=16
#SBATCH --mem=80G
#SBATCH --time=2-00:00:00
#SBATCH --mail-user=suchap@cvut.cz
#SBATCH --partition=compute
#SBATCH --mail-type=ALL

#module load Anaconda3/5.3.0
module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0

pip3 install numpy --user

#. /opt/apps/software/Anaconda3/5.3.0/etc/profile.d/conda.sh

cd /home/suchap/dev/Prevedig/python_scripts/prevedig/
python ILP_biochem_throughput_maxim_fixed_transport.py 1400 108000
