#!/bin/bash

#SBATCH --job-name=single
#SBATCH --output="/home/koutepa2/advance-or-scheduling-with-ml/instances/bug/output.out"
#SBATCH --error="/home/koutepa2/advance-or-scheduling-with-ml/instances/bug/output.err"
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=16G
#SBATCH --time=4-00:00:00
#SBATCH --partition=compute,bigmem

module purge
module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0
module load SciPy-bundle/2019.10-fosscuda-2019b-Python-3.7.4
pip3 install lightgbm --user
pip3 install PyYAML --user

FOLDER="/home/koutepa2/advance-or-scheduling-with-ml/instances/bug/"
cd /home/koutepa2/advance-or-scheduling-with-ml
python validate_instances.py -f "$FOLDER"


