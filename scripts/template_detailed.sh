#!/bin/bash -l

# Give your job a name, so you can recognize it in the queue overview
#SBATCH --job-name=test_or

# Define, how many nodes you need.
#SBATCH --nodes=2
#SBATCH --ntasks=4 	# ask for 4 cpus

# Define, how long the job will run in real time. 
#              d-hh:mm:ss
#SBATCH --time=2-00:00:00

# Define the partition on which the job shall run. May be omitted.
#SBATCH --partition gpu

# How much memory you need. Choose one of those.
#SBATCH --mem-per-cpu=5000MB  	# defines memory per CPU/core
##SBATCH --mem=5GB   		# defines memory per node

# Turn on mail notification.
#SBATCH --mail-type=END,FAIL

# You may not place any commands before the last SBATCH directive

# Define and create a unique scratch directory for this job
# /lscratch is local ssd disk on particular node which is faster than your network home dir
SCRATCH_DIRECTORY=/lscratch/${USER}/${SLURM_JOBID}.stallo-adm.uit.no
mkdir -p ${SCRATCH_DIRECTORY}
cd ${SCRATCH_DIRECTORY}

# You can copy everything you need to the scratch directory
# ${SLURM_SUBMIT_DIR} points to the path where this script was submitted from
cp ${SLURM_SUBMIT_DIR}/myfiles*.txt ${SCRATCH_DIRECTORY}

# This is where the actual work is done. In this case, the script only waits.
# The time command is optional, but it may give you a hint on how long the command worked.
time sleep 150
#sleep 150

# After the job is done we copy our output back to $SLURM_SUBMIT_DIR
cp ${SCRATCH_DIRECTORY}/my_output ${SLURM_SUBMIT_DIR}

# After everything is saved to the home directory, delete the work directory to
# save space on /lscratch
cd ${SLURM_SUBMIT_DIR}
rm -rf ${SCRATCH_DIRECTORY}

# Finish the script
exit 0
