"""
This file declares all the classes needed for the column generation algorithm to run properly - patterns
to be operated with, master problem and subproblem.
"""

import utilities
import gurobipy as g
import numpy as np
import random


class Pattern:
    """
    A class used to represent a pattern that is used in the column generation algorithm. Pattern represents
    a possible patient-block assignment.
    """
    def __init__(self, id, block, patients, surgeons, overtime, undertime, number_of_blocks, number_of_patients, number_of_surgeons):
        """
        :param id: ID of the pattern
        :param block: block used in this pattern for the patient-block assignment
        :param patients: set of patients used in this pattern for the patient-block assignment
        :param surgeons: set of surgeons used in this pattern for the patient-block assignment
        :param overtime: total overtime of the pattern
        :param undertime: total undertime of the pattern (unutilized capacity of the pattern according to the length of a block)
        :param number_of_blocks: total number of blocks in the instance
        :param number_of_patients: total number of patients in the instance
        :param number_of_surgeons: total number of surgeons in the instance
        """
        self.id = id
        self.block = block
        self.patients = patients
        self.surgeons = surgeons
        self.block_vec = [1 if i == block.id else 0 for i in range(number_of_blocks)]
        self.patient_vec = [1 if i in [patient.id for patient in patients] else 0 for i in range(number_of_patients)]
        self.surgeon_vec = [1 if i in [surgeon.id for surgeon in surgeons] else 0 for i in range(number_of_surgeons)]
        self.patient_id = [patient.id for patient in self.patients]
        self.surgeon_id = [surgeon.id for surgeon in self.surgeons]
        self.overtime = overtime
        self.undertime = undertime

    def print_pattern(self):
        """
        This function prints one pattern in the format used by the scheduling algorithms.
        """
        print(f"| PATTERN: {self.id} | BLOCKS: {[self.block.id]} | PATIENTS: {self.patient_id} | SURGEONS: {self.surgeon_id} | OVERTIME: {self.overtime} | UNDERTIME: {self.undertime}")


class MasterProblemPrimal:
    """
    A class used for representing primal master problem and solving it.
    """
    def __init__(self, instance, branching_constraints, idx, objective_value=None, primal_variables=None, dual_variables=None):
        """
        :param instance: instance on which the OR scheduling task is performed
        :param branching_constraints: branching constraints defined for this node in the branching tree
        :param idx: index of the MP node (what node in the branching tree are we dealing with)
        :param objective_value: objective value of the MP node
        :param primal_variables: primal variables of the MP node
        :param dual_variables: dual variables of the MP node
        """
        self.idx = idx
        self.instance, self.model = instance, None
        self.branching_constraints = branching_constraints
        self.objective_value, self.heuristic_objective_value = objective_value, None
        self.primal_variables, self.dual_variables, self.heuristic_variables = primal_variables, dual_variables, None
        self.optimality_gap = 0

    def optimize_ilp(self, timelimit, bound):
        """
        This function defines the desired ILP model with all the constraints, optimizes it and stores the
        objective value and variable values to the class variables.

        :param timelimit: time limit for the optimize method
        :param bound: upper bound for the model
        """

        # create empty model
        self.model = g.Model()
        self.model.setParam(g.GRB.Param.OutputFlag, 0)
        self.model.setParam(g.GRB.Param.Seed, 1)
        self.model.setParam(g.GRB.Param.TimeLimit, timelimit)
        self.model.setParam(g.GRB.Param.Presolve, 0)
        self.model.setParam(g.GRB.Param.MIPGap, 1e-6)
        self.model.setParam(g.GRB.Param.IntFeasTol, 1e-6)

        # create variables
        x = self.model.addVars(self.instance.n_patients, self.instance.n_blocks, vtype=g.GRB.BINARY, name="x")
        o = self.model.addVars(self.instance.n_blocks, lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="o")
        z = self.model.addVars(self.instance.n_blocks, lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="z")
        n = self.model.addVars(self.instance.n_surgeons, self.instance.n_days, vtype=g.GRB.BINARY, name="n")
        y = self.model.addVars(self.instance.n_patients + 1, self.instance.n_patients + 1, self.instance.n_blocks, vtype=g.GRB.BINARY, name="y")
        e = self.model.addVars(self.instance.n_blocks, vtype=g.GRB.BINARY, name="e")

        # add constraints
        for patient in self.instance.patients:
            self.model.addConstr(g.LinExpr((1, x[patient.id, block.id]) for block in self.instance.blocks) <= 1, name=f"constraint_1_p{patient.id}")
        for block in self.instance.blocks:
            self.model.addConstr(g.LinExpr((patient.duration, x[patient.id, block.id]) for patient in self.instance.patients) + g.LinExpr((self.instance.sequence_duration[patient_1.group][patient_2.group], y[patient_1.id, patient_2.id, block.id]) for patient_1 in self.instance.patients for patient_2 in self.instance.patients if patient_2.id != patient_1.id) - block.capacity == o[block.id] - z[block.id], name=f"constraint_2_b{block.id}")
        self.model.addConstrs((o[block.id] <= block.overtime for block in self.instance.blocks), name="constraint_3")
        for room in self.instance.rooms:
            for day in self.instance.days:
                self.model.addConstr(g.LinExpr((1, o[block.id]) for block in self.instance.blocks if block.day == day) <= room.overtime, name=f"constraint_4_r{room.id}_d{day}")
        for surgeon in self.instance.surgeons:
            for day in self.instance.days:
                self.model.addConstr(g.LinExpr((1, x[patient.id, block.id]) for block in self.instance.blocks if block.day == day for patient in self.instance.patients if patient.surgeon == surgeon.id) <= self.instance.max_patients_surgeon_day[surgeon.id] * n[surgeon.id, day], name=f"constraint_5_s{surgeon.id}_d{day}")
        self.model.addConstrs((x[patient.id, block.id] == 0 for block in self.instance.blocks for patient in self.instance.patients if sum([1 if patient.surgeon == surgeon.id and block.id in surgeon.blocks else 0 for surgeon in self.instance.surgeons]) == 0), name="constraint_6")
        self.model.addConstrs((x[patient.id, block.id] == 0 for patient in self.instance.patients for block in self.instance.blocks if patient.release > block.day), name="constraint_7")

        for patient in self.instance.patients:
            for block in self.instance.blocks:
                self.model.addConstr(x[patient.id, block.id] == g.LinExpr((1, y[patient.id, patient_next.id, block.id]) for patient_next in self.instance.patients if patient_next.id != patient.id) + y[patient.id, self.instance.n_patients, block.id])
                self.model.addConstr(x[patient.id, block.id] == g.LinExpr((1, y[patient_prev.id, patient.id, block.id]) for patient_prev in self.instance.patients if patient_prev.id != patient.id) + y[self.instance.n_patients, patient.id, block.id])
                self.model.addConstr(x[patient.id, block.id] <= e[block.id])
        for block in self.instance.blocks:
            self.model.addConstr(g.LinExpr((1, y[patient.id, self.instance.n_patients, block.id]) for patient in self.instance.patients) == e[block.id])
            self.model.addConstr(g.LinExpr((1, y[self.instance.n_patients, patient.id, block.id]) for patient in self.instance.patients) == e[block.id])
            self.model.addConstr(e[block.id] <= g.LinExpr((1, x[patient.id, block.id]) for patient in self.instance.patients))

        # add surgeon-day branching constraints
        for branching_constraint in self.branching_constraints['surgeon']:
            branching_surgeon_id, branching_day_id, indication = branching_constraint[0], branching_constraint[1], branching_constraint[2]
            if indication == 0:
                self.model.addConstr((n[branching_surgeon_id, branching_day_id] == 0), name=f"surgeon_branching_constraint_{branching_surgeon_id}_{branching_day_id}")
            if indication == 1:
                self.model.addConstr((n[branching_surgeon_id, branching_day_id] == 1), name=f"surgeon_branching_constraint_{branching_surgeon_id}_{branching_day_id}")

        # set objective
        obj1 = g.LinExpr(((day - patient.release) * patient.priority, x[patient.id, block.id]) for patient in self.instance.patients for day in self.instance.days for block in self.instance.blocks if block.day == day)
        obj5 = g.LinExpr((1, x[patient.id, block.id]) for patient in self.instance.patients for block in self.instance.blocks)
        obj6 = g.LinExpr((1, n[surgeon.id, day]) for surgeon in self.instance.surgeons for day in self.instance.days)
        obj7 = g.quicksum(o[block.id] * o[block.id] for block in self.instance.blocks)
        obj8 = g.quicksum(z[block.id] * z[block.id] for block in self.instance.blocks)
        objective = self.instance.weights['m1'] * obj1 - self.instance.weights['m5'] * obj5 + self.instance.weights['m6'] * obj6 + self.instance.weights['m7'] * obj7 + self.instance.weights['m7'] * obj8
        self.model.setObjective(objective, sense=g.GRB.MINIMIZE)

        # set the model bound according to the best ILP solution we currently have
        self.model.setParam("Cutoff", bound)

        # update the model
        self.model.update()

        # optimize the model
        self.model.optimize()
        print(f'BaP ILP model status: {self.model.status}', flush=True)

        # if the model is infeasible or cut off, exit the function
        if (self.model.status != g.GRB.OPTIMAL and self.model.status != g.GRB.TIME_LIMIT) or self.model.SolCount == 0:
            return

        # store optimality gap
        self.optimality_gap = self.model.MIPGap

        # store objective value
        self.objective_value = round(float(self.model.objVal), 1)

        # store all the model variables
        self.primal_variables = {
            'x': utilities.convert_dictionary_to_array(self.model.getAttr('X', x), np.zeros((self.instance.n_patients, self.instance.n_blocks))),
            'o': self.model.getAttr('X', o.values()),
            'z': self.model.getAttr('X', z.values()),
            'n': utilities.convert_dictionary_to_array(self.model.getAttr('X', n), np.zeros((self.instance.n_surgeons, self.instance.n_days)))
        }

    def optimize_model(self, timelimit, integer, bound=None):
        """
        This function defines the desired model for MP with all the constraints, optimizes it and stores the
        objective value and variable values to the class variables. It is possible to solve it in relaxed (master
        problem) or integer (master heuristic) way. It also stores the dual variables.

        :param timelimit: time limit for the optimize method
        :param integer: should the model be solved to integrality? (True/False)
        :param bound: upper bound for the master heuristic model
        """

        # create empty model
        self.model = g.Model()
        self.model.setParam(g.GRB.Param.OutputFlag, 0)
        self.model.setParam(g.GRB.Param.Seed, 1)
        self.model.setParam(g.GRB.Param.TimeLimit, timelimit)
        self.model.setParam(g.GRB.Param.Presolve, 0)
        self.model.setParam(g.GRB.Param.MIPGap, 1e-6)
        self.model.setParam(g.GRB.Param.IntFeasTol, 1e-6)

        # create variables
        if integer:
            theta = self.model.addVars(len(self.instance.patterns), vtype=g.GRB.BINARY, name="theta")
            n = self.model.addVars(self.instance.n_surgeons, self.instance.n_days, vtype=g.GRB.BINARY, name="n")
        else:
            theta = self.model.addVars(len(self.instance.patterns), lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.CONTINUOUS, name="theta")
            n = self.model.addVars(self.instance.n_surgeons, self.instance.n_days, lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.CONTINUOUS, name="n")

        # set constraints
        for patient in self.instance.patients:
            self.model.addConstr(g.LinExpr(((1 if patient.id in pattern.patient_id and pattern.block.day == day else 0), theta[idx]) for idx, pattern in enumerate(self.instance.patterns) for day in self.instance.days) <= 1, name=f'constraint_1_p{patient.id}')
        for block in self.instance.blocks:
            self.model.addConstr(g.LinExpr(((1 if pattern.block.id == block.id else 0), theta[idx]) for idx, pattern in enumerate(self.instance.patterns)) == 1, name=f'constraint_2_b{block.id}')
        for room in self.instance.rooms:
            for day in self.instance.days:
                self.model.addConstr((g.LinExpr(((pattern.overtime if pattern.block.room == room.id and pattern.block.day == day else 0), theta[idx]) for idx, pattern in enumerate(self.instance.patterns)) <= room.overtime), name=f'constraint_3_r{room.id}_d{day}')
        for surgeon in self.instance.surgeons:
            for day in self.instance.days:
                self.model.addConstr((g.LinExpr(((1 if surgeon.id in pattern.surgeon_id and pattern.block.day == day else 0), theta[idx]) for idx, pattern in enumerate(self.instance.patterns)) <= self.instance.max_blocks_surgeon_day * n[surgeon.id, day]), name=f'constraint_4_s{surgeon.id}_d{day}')

        # add surgeon-day branching constraints
        for branching_constraint in self.branching_constraints['surgeon']:
            branching_surgeon_id, branching_day_id, indication = branching_constraint[0], branching_constraint[1], branching_constraint[2]
            if indication == 0:
                self.model.addConstr((n[branching_surgeon_id, branching_day_id] == 0), name=f"surgeon_branching_constraint_{branching_surgeon_id}_{branching_day_id}")
            if indication == 1:
                self.model.addConstr((n[branching_surgeon_id, branching_day_id] == 1), name=f"surgeon_branching_constraint_{branching_surgeon_id}_{branching_day_id}")

        # set objective
        obj1 = g.LinExpr(((day - patient.release) * patient.priority * (1 if patient.id in pattern.patient_id and pattern.block.day == day else 0), theta[idx]) for patient in self.instance.patients for day in self.instance.days for idx, pattern in enumerate(self.instance.patterns))
        obj5 = g.LinExpr((1 if patient.id in pattern.patient_id and pattern.block.day == day else 0, theta[idx]) for patient in self.instance.patients for day in self.instance.days for idx, pattern in enumerate(self.instance.patterns))
        obj6 = g.LinExpr((1, n[surgeon.id, day]) for surgeon in self.instance.surgeons for day in self.instance.days)
        obj7 = g.LinExpr((pattern.overtime**2 if pattern.block.room == room.id and pattern.block.day == day else 0, theta[idx]) for room in self.instance.rooms for day in self.instance.days for idx, pattern in enumerate(self.instance.patterns))
        obj8 = g.LinExpr((pattern.undertime**2 if pattern.block.room == room.id and pattern.block.day == day else 0, theta[idx]) for room in self.instance.rooms for day in self.instance.days for idx, pattern in enumerate(self.instance.patterns))
        obj = self.instance.weights['m1'] * obj1 - self.instance.weights['m5'] * obj5 + self.instance.weights['m6'] * obj6 + self.instance.weights['m7'] * obj7 + self.instance.weights['m7'] * obj8
        self.model.setObjective(obj, sense=g.GRB.MINIMIZE)

        # set the model bound according to the best ILP solution we currently have
        if integer:
            self.model.setParam("Cutoff", bound)

        # update the model
        self.model.update()

        # optimize the model
        self.model.optimize()

        # if the model is infeasible or cut off, exit the function
        if (self.model.status != g.GRB.OPTIMAL and self.model.status != g.GRB.TIME_LIMIT) or self.model.SolCount == 0:
            return

        # store objective value
        objective_value = round(float(self.model.objVal), 1)

        # store optimality gap
        if integer:
            self.optimality_gap = self.model.MIPGap

        # store primal variables to dictionary
        primal_variables = {
            'theta': {idx: theta for idx, theta in zip([p.id for p in self.instance.patterns], self.model.getAttr('X', theta.values()))},
            'n': utilities.convert_dictionary_to_array(self.model.getAttr('X', n), np.zeros((self.instance.n_surgeons, self.instance.n_days)))
        }

        if integer:
            self.heuristic_objective_value = objective_value
            self.heuristic_variables = primal_variables
        else:
            self.objective_value = objective_value
            self.primal_variables = primal_variables

            # store also dual variables to dictionary
            dual_variables = {
                'lambda': {patient.id: self.model.getConstrByName(f"constraint_1_p{patient.id}").getAttr('Pi') for patient in self.instance.patients},
                'mu': {block.id: self.model.getConstrByName(f"constraint_2_b{block.id}").getAttr('Pi') for block in self.instance.blocks},
                'xi': utilities.convert_dictionary_to_array(
                    {(room.id, day): self.model.getConstrByName(f"constraint_3_r{room.id}_d{day}").getAttr('Pi') for room in self.instance.rooms for day
                     in self.instance.days},
                    np.zeros((self.instance.n_rooms, self.instance.n_days))),
                'zeta': utilities.convert_dictionary_to_array(
                    {(surgeon.id, day): self.model.getConstrByName(f"constraint_4_s{surgeon.id}_d{day}").getAttr('Pi') for surgeon in
                     self.instance.surgeons for day in
                     self.instance.days}, np.zeros((self.instance.n_surgeons, self.instance.n_days)))
            }
            self.dual_variables = dual_variables

    def find_fixing_combinations(self, z_ilp_best):
        """
        This function finds variables that can be fixed according to the reduced cost fixing
        technique condition (z_LP + reduced_cost_x > z_ILP_best).

        :param z_ilp_best: objective value of the best integer solution we currently have
        :return: variables to be fixed (surgeon-day combinations)
        """

        fixing_combinations = []
        z_lp = self.model.objVal

        for surgeon in self.instance.surgeons:
            for day in self.instance.days:
                reduced_cost = self.model.getVarByName(f'n[{surgeon.id},{day}]').getAttr('RC')
                variable_value = round(self.model.getVarByName(f'n[{surgeon.id},{day}]').X, 2)
                if z_lp + reduced_cost > z_ilp_best and variable_value == 0:
                    fixing_combinations.append((surgeon.id, day))

        return fixing_combinations

    def check_fractionality(self):
        """
        This function observes if there is any fractional patient-block or surgeon-day combination.

        :return: fractional patient and fractional surgeon indication
        """

        is_fractional_patient, is_fractional_surgeon = False, False
        epsilon = 1e-2

        # check for a fractionality of patient
        for key, value in self.primal_variables['theta'].items():
            if - epsilon <= value <= epsilon or (1 - epsilon) <= value <= (1 + epsilon):
                continue
            is_fractional_patient = True
            break

        # check for a fractionality of surgeon
        for value in np.nditer(self.primal_variables['n']):
            if - epsilon <= value <= epsilon or (1 - epsilon) <= value <= (1 + epsilon):
                continue
            is_fractional_surgeon = True
            break

        return is_fractional_patient, is_fractional_surgeon

    def find_fractional_variable(self, selection_type, variable_type):
        """
        This function returns a fractional patient-block, surgeon-day or aggregated surgeon
        combination according to the chosen variable type. Currently, we can choose from
        multiple strategies for selecting fractional variable:
            > most fractional: returns fractional variable that is most fractional
            > closest to integer: returns fractional variable that is closest to integer
            > random order: returns random fractional variable
            > logical order: returns first fractional variable

        :param selection_type: strategy for selecting fractional variable ('most fractional' /
                               'closest to integer' / 'random order' / 'logical order')
        :param variable_type: type of variable where fractionality should be found ('patient' / 'surgeon')
        :return: fractional variable
        """

        # 1. collect all the fractional variables
        variables, values = [], []

        # search for a fractional patient-block combination
        if variable_type == 'patient':
            for block in self.instance.blocks:
                patterns = [p for p in self.instance.patterns if p.block == block]
                for patient in self.instance.patients:
                    value = round(sum([(1 if patient.id in pattern.patient_id else 0) * self.primal_variables['theta'][pattern.id] for pattern in patterns]), 2)
                    if not value.is_integer():
                        variables.append([patient.id, block.id])
                        values.append(value)

        # search for a fractional surgeon-day combination
        elif variable_type == 'surgeon':
            for surgeon in self.instance.surgeons:
                for day in self.instance.days:
                    value = round(self.primal_variables['n'][surgeon.id, day], 2)
                    if not value.is_integer():
                        variables.append([surgeon.id, day])
                        values.append(value)

        # 2. select one of the fractional variables for a new branching rule
        if selection_type == 'most fractional':
            values_np = np.asarray(values)
            idx = (np.abs(values_np - 0.5)).argmin()
            return variables[idx]

        elif selection_type == 'closest to integer':
            values_np = np.asarray(values)
            vals = [(np.abs(values_np - 0)).min(), (np.abs(values_np - 0)).min()]
            indices = [(np.abs(values_np - 0)).argmin(), (np.abs(values_np - 0)).argmin()]
            idx = indices[np.asarray(vals).argmin()]
            return variables[idx]

        elif selection_type == 'random order':
            idx = random.randint(0, len(variables) - 1)
            return variables[idx]

        elif selection_type == 'logical order':
            return variables[0]


class Subproblem:
    """
    A class used for representing subproblem in column generation and solving it.
    """
    def __init__(self, instance):
        """
        :param instance: instance on which the OR scheduling task is performed
        """
        self.instance, self.model = instance, None
        self.x, self.o, self.n = None, None, None
        self.objective_value = None
        self.variables = {}

    def optimize_model(self, timelimit, master_problem_variables, branching_constraints, block, day, room):
        """
        This function defines the desired model for subproblem with all the constraints, optimizes it and stores the
        objective value and variable values to the class variables.

        :timelimit: time limit for the optimize method
        :param master_problem_variables: variables of optimized MP
        :param branching_constraints: patient branching constraints
        :param block: block for which the subproblem is optimized
        :param day: day for which the subproblem is optimized
        :param room: room for which the subproblem is optimized
        """

        # initialize the model itself and silent outputs of the model
        self.model = g.Model()
        self.model.setParam(g.GRB.Param.OutputFlag, 0)
        self.model.setParam(g.GRB.Param.Seed, 1)
        self.model.setParam(g.GRB.Param.MIPGap, 1e-6)
        self.model.setParam(g.GRB.Param.IntFeasTol, 1e-6)

        # create variables
        self.x = self.model.addVars(self.instance.n_patients, vtype=g.GRB.BINARY, name="x")
        self.o = self.model.addVar(lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="o")
        self.z = self.model.addVar(lb=0, ub=g.GRB.INFINITY, vtype=g.GRB.INTEGER, name="z")
        self.n = self.model.addVars(self.instance.n_surgeons, vtype=g.GRB.BINARY, name="n")
        self.y = self.model.addVars(self.instance.n_patients + 1, self.instance.n_patients + 1, vtype=g.GRB.BINARY, name="y")
        self.e = self.model.addVar(vtype=g.GRB.BINARY, name="e")

        # add initial constraints
        self.model.addConstr(g.LinExpr((patient.duration, self.x[patient.id]) for patient in self.instance.patients) + g.LinExpr((self.instance.sequence_duration[patient_1.group][patient_2.group], self.y[patient_1.id, patient_2.id]) for patient_1 in self.instance.patients for patient_2 in self.instance.patients if patient_2.id != patient_1.id) - block.capacity == self.o - self.z, name=f"constraint_1")
        self.model.addConstr(self.o <= block.overtime, name="constraint_2")
        self.model.addConstrs((self.x[patient.id] <= self.n[surgeon.id] for surgeon in self.instance.surgeons if block.id in surgeon.blocks for patient in self.instance.patients if patient.surgeon == surgeon.id), name="constraint_3")
        self.model.addConstrs((self.x[patient.id] == 0 for patient in self.instance.patients if sum([1 if patient.surgeon == surgeon.id and block.id in surgeon.blocks else 0 for surgeon in self.instance.surgeons]) == 0), name="constraint_4")
        self.model.addConstrs((self.x[patient.id] == 0 for patient in self.instance.patients if patient.release > day), name="constraint_5")
        self.model.addConstrs((self.n[surgeon.id] == 0 for surgeon in self.instance.surgeons if block.id not in surgeon.blocks), name="constraint_6")

        for patient in self.instance.patients:
            self.model.addConstr(self.x[patient.id] == g.LinExpr((1, self.y[patient.id, patient_next.id]) for patient_next in self.instance.patients if patient_next.id != patient.id) + self.y[patient.id, self.instance.n_patients])
            self.model.addConstr(self.x[patient.id] == g.LinExpr((1, self.y[patient_prev.id, patient.id]) for patient_prev in self.instance.patients if patient_prev.id != patient.id) + self.y[self.instance.n_patients, patient.id])
            self.model.addConstr(self.x[patient.id] <= self.e)
        self.model.addConstr(g.LinExpr((1, self.y[patient.id, self.instance.n_patients]) for patient in self.instance.patients) == self.e)
        self.model.addConstr(g.LinExpr((1, self.y[self.instance.n_patients, patient.id]) for patient in self.instance.patients) == self.e)
        self.model.addConstr(self.e <= g.LinExpr((1, self.x[patient.id]) for patient in self.instance.patients))

        # add patient-block branching constraints
        for branching_constraint in branching_constraints['patient']:
            branching_patient_id, branching_block_id, indication = branching_constraint[0], branching_constraint[1], branching_constraint[2]
            if block.id == branching_block_id:  # operating with block b
                if indication == 0:  # 'patient p not assigned to block b'
                    self.model.addConstr((self.x[branching_patient_id] == 0), name=f"patient_branching_constraint_{branching_patient_id}_{branching_block_id}")
                if indication == 1:  # 'patient p assigned to block b'
                    self.model.addConstr((self.x[branching_patient_id] == 1), name=f"patient_branching_constraint_{branching_patient_id}_{branching_block_id}")
            if block.id != branching_block_id:  # operating with block that is not b
                if indication == 1:  # 'patient p assigned to block b'
                    self.model.addConstr((self.x[branching_patient_id] == 0), name=f"patient_branching_constraint_{branching_patient_id}_{branching_block_id}")

        # add surgeon-day branching constraints (only n_sd = 1 as we cannot assign a surgeon to specific block, only day)
        for branching_constraint in branching_constraints['surgeon']:
            branching_surgeon_id, branching_day_id, indication = branching_constraint[0], branching_constraint[1], branching_constraint[2]
            if indication == 0 and branching_day_id == day:
                self.model.addConstr((self.n[branching_surgeon_id] == 0), name=f"surgeon_branching_constraint_{branching_surgeon_id}_{branching_day_id}")

        # set objective
        part1 = g.LinExpr((master_problem_variables['lambda'][patient.id], self.x[patient.id]) for patient in self.instance.patients) \
                + master_problem_variables['mu'][block.id] \
                + master_problem_variables['xi'][room.id, day] * self.o \
                + g.LinExpr((master_problem_variables['zeta'][surgeon.id, day], self.n[surgeon.id]) for surgeon in self.instance.surgeons if block.id in surgeon.blocks)
        part2 = g.LinExpr(((day - patient.release) * patient.priority, self.x[patient.id]) for patient in self.instance.patients)
        part3 = g.LinExpr((1, self.x[patient.id]) for patient in self.instance.patients)
        part4 = self.o * self.o
        part5 = self.z * self.z
        objective = part1 - self.instance.weights['m1'] * part2 + self.instance.weights['m5'] * part3 - self.instance.weights['m7'] * part4 - self.instance.weights['m7'] * part5
        self.model.setObjective(objective, sense=g.GRB.MAXIMIZE)

        # set the model bound as we are only interested with subproblems having objective value > 0
        self.model.setParam("Cutoff", 0)

        # update the model
        self.model.update()

        # optimize model
        self.model.optimize()

        # if the model is infeasible, exit the function
        if self.model.status != g.GRB.OPTIMAL:
            return

        # store objective value
        self.objective_value = round(float(self.model.objVal), 1)

        # store optimized variables to dictionary
        self.variables = {
            'x': self.model.getAttr('X', self.x.values()),
            'o': self.o.X,
            'z': self.z.X,
            'n': self.model.getAttr('X', self.n.values())
        }
